Introduction
====================

This project adheres to the Contributor Covenant [code of conduct](CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code. Please report unacceptable behavior to [dnet-team@isti.cnr.it](mailto:dnet-team@isti.cnr.it).

This project is licensed under the [AGPL v3 or later version](#LICENSE.md).

Purpose
====================

This project defines **object schemas** of the OpenAIRE main entities and the relationships that intercur among them. 
Namely it defines the model for 

- the graph internal representation, defined under the package `eu.dnetlib.dhp.schema.oaf`
- the scholexplorer content representation, defined under the package `eu.dnetlib.dhp.schema.sx`
- the contents acquired from the metadata aggregation subsystem, defined under the package `eu.dnetlib.dhp.schema.mdstore`
- the ORCID common schemas, defined under the package `eu.dnetlib.dhp.schema.orcid`
- the Solr common schemas used to represent the information returned to the Explore portal and the APIs, defined under 
the package `eu.dnetlib.dhp.schema.solr`
 
The serialization of such objects (data store files) are used to pass data between workflow nodes in the processing pipeline 
and / or intended to be shared across components.
