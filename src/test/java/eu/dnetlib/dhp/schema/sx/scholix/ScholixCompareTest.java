package eu.dnetlib.dhp.schema.sx.scholix;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

public class ScholixCompareTest {

    private ScholixIdentifier generateMockScholixId(final String baseId ,boolean toUpper, int idCount) {

        final String id = String.format("%s.%d",baseId, idCount);
        final String schema = "DOI";
        final String url =String.format("http://dx.dOI.org/%s.%d",baseId, idCount);

        final ScholixIdentifier result = new ScholixIdentifier();
        result.setIdentifier(toUpper ? id.toUpperCase(): id.toLowerCase());
        result.setSchema(toUpper ? schema.toUpperCase():schema.toLowerCase());
        result.setUrl(toUpper ? url.toUpperCase():url.toLowerCase());
        return result;
    }

    private ScholixIdentifier generateMockScholixId(boolean toUpper, int idCount) {
        return generateMockScholixId("10.11646/zootaxa.5099.1", toUpper, idCount);
    }

    private ScholixEntityId generateMockScholixEntityId(String name, boolean toUpper, boolean invertOrder, int numberOfIds) {


        final List<ScholixIdentifier> ids = new ArrayList<>();

        if (!invertOrder) {
            for (int i = 0; i < numberOfIds; i++) {
                ids.add(generateMockScholixId(toUpper, i));
            }
        }
        else {
            for (int i = numberOfIds-1; i >=0; i--) {
                ids.add(generateMockScholixId(toUpper, i));
            }
        }

        return new ScholixEntityId(toUpper? name.toUpperCase(): name.toLowerCase(), ids);
    }

    private ScholixEntityId generateMockScholixEntityId(boolean toUpper, boolean invertOrder, int numberOfIds) {

        return generateMockScholixEntityId("datacite", toUpper, invertOrder,numberOfIds);
    }

    private ScholixCollectedFrom generateMockScholixCollectedFrom(final String dsName , final boolean toUpper, final boolean invertOrder, final int numberOfIds) {

        final ScholixCollectedFrom result = new ScholixCollectedFrom();
        final String completionStatus = "complete";
        final String provisionMode = "collected";
        result.setProvider(generateMockScholixEntityId(dsName,toUpper, invertOrder, numberOfIds));
        result.setCompletionStatus(toUpper ? completionStatus.toUpperCase(): completionStatus.toLowerCase());
        result.setProvisionMode(toUpper ? provisionMode.toUpperCase(): provisionMode.toLowerCase());
        return result;
    }


    private ScholixCollectedFrom generateMockScholixCollectedFrom(boolean toUpper, boolean invertOrder, int numberOfIds) {
        return  generateMockScholixCollectedFrom("datasource", toUpper, invertOrder,numberOfIds);
    }

    private ScholixRelationship generateMockScholixRelationships(boolean toUpper) {

        final String name = "IsRelatedTo";
        final String inverse = "RelatedTo";
        final String schema = "datacite";

        final ScholixRelationship rel = new ScholixRelationship();

        rel.setName(toUpper? name.toUpperCase():name.toLowerCase());
        rel.setInverse(toUpper? inverse.toUpperCase():inverse.toLowerCase());
        rel.setSchema(toUpper? schema.toUpperCase():schema.toLowerCase());

        return rel;
    }


    private ScholixResource generateMockScholixResource(final String sourceBase, boolean toUpper, int idCount, boolean invertOrder, int numEntityIds) {
        final ScholixResource resource = new ScholixResource();
        final String baseSourceId = "%s_10.1000/ID_%d/";
        final String creatorBase ="Creator %d";
        final String publisherBase ="Publisher %d";
        final String collectedFromBase ="Datasource %d";
        final String title = String.format("Resource %s Title 1", sourceBase);
        final List<ScholixIdentifier> ids = IntStream.range(0,numEntityIds)
                .mapToObj(i -> String.format(baseSourceId,sourceBase, i))
                .map(s ->generateMockScholixId(s, toUpper, idCount))
                        .collect(Collectors.toList());
        resource.setIdentifier(ids);
        resource.setDnetIdentifier("dnetId");

        resource.setObjectType("dataset");
        resource.setObjectSubType("dataset");
        resource.setTitle(title);
        final  List<ScholixEntityId> creators = IntStream.range(0,numEntityIds)
                .mapToObj(i -> String.format(creatorBase, i))
                .map(s ->generateMockScholixEntityId(s, toUpper,invertOrder,idCount))
                .collect(Collectors.toList());
        resource.setCreator(creators);
        resource.setPublicationDate("22-02-2022");


        final  List<ScholixEntityId> publishers = IntStream.range(0,numEntityIds)
                .mapToObj(i -> String.format(publisherBase, i))
                .map(s ->generateMockScholixEntityId(s, toUpper,invertOrder,idCount))
                .collect(Collectors.toList());
        resource.setPublisher(publishers);


        final List<ScholixCollectedFrom> collectedFroms = IntStream.range(0,numEntityIds)
                .mapToObj(i -> String.format(collectedFromBase, i))
                .map(s -> generateMockScholixCollectedFrom(s,toUpper, invertOrder, idCount))
                .collect(Collectors.toList());

        resource.setCollectedFrom(collectedFroms);
        return resource;
    }



    private Scholix generateMockScholix( boolean toUpper, int idCount, boolean invertOrder, int numEntityIds) {
        final Scholix result = new Scholix();
        result.setPublicationDate("2022-02-22");

        final String id = "dnetID";
        final String publisherBase ="Publisher %d";

        result.setPublisher(IntStream.range(0,numEntityIds)
                .mapToObj(i -> String.format(publisherBase, i))
                .map(s ->generateMockScholixEntityId(s, toUpper,invertOrder,idCount))
                .collect(Collectors.toList()));

        result.setLinkprovider(IntStream.range(0,numEntityIds)
                .mapToObj(i -> String.format(publisherBase, i))
                .map(s ->generateMockScholixEntityId(s, toUpper,invertOrder,idCount))
                .collect(Collectors.toList()));

        result.setRelationship(generateMockScholixRelationships(toUpper));
        result.setSource(generateMockScholixResource("source", toUpper, idCount,invertOrder, numEntityIds));
        result.setTarget(generateMockScholixResource("target", toUpper, idCount,invertOrder, numEntityIds));

        result.setIdentifier(toUpper?id.toUpperCase():id.toLowerCase());
        return result;
    }



    @Test
    public void testScholixIdentifierComparison() {

        final ScholixIdentifier left = generateMockScholixId(true, 1);
        final ScholixIdentifier right = generateMockScholixId(false,1);
        assertEquals(0,left.compareTo(right));
        assertEquals(left, right);
        assertEquals(left.hashCode(), right.hashCode());
        left.setIdentifier(null);
        assertEquals(-1, left.compareTo(right));
    }


    @Test
    public void testScholixEntityIDComparison() {

        final ScholixEntityId first =generateMockScholixEntityId(true,false,10);
        final ScholixEntityId second =generateMockScholixEntityId(false,true,10);
        assertEquals(first,second);
        assertEquals(first.hashCode(), second.hashCode());

    }


    @Test
    public void testScholixCollectedFromComparison() {
        final ScholixCollectedFrom cfLeft = generateMockScholixCollectedFrom(true, true, 20);

        final ScholixCollectedFrom cfRight = generateMockScholixCollectedFrom(false, false, 20);

        assertEquals(cfLeft, cfRight);

        assertEquals(cfLeft.hashCode(), cfRight.hashCode());

        cfRight.setCompletionStatus(null);

        assertNotEquals(cfLeft, cfRight);
    }

    @Test
    public void testCompareScholixResource() {
        final ScholixResource left = generateMockScholixResource("source",true,5, true,5);
        final ScholixResource right = generateMockScholixResource("source",false,5, false,5);
        assertEquals(left,right);
        assertEquals(left.hashCode(), right.hashCode());
        final ScholixResource different = generateMockScholixResource("source",false,4, false,5);
        assertNotEquals(different,right);
        assertNotEquals(different,left);
        assertNotEquals(left.hashCode(), different.hashCode());
        assertNotEquals(right.hashCode(), different.hashCode());
    }


    @Test
    public void testCompareScholix() {

        final Scholix left = generateMockScholix(true,5, true,5);
        final Scholix right = generateMockScholix(false,5, false,5);
        assertEquals(left,right);
        assertEquals(left.hashCode(), right.hashCode());
        final Scholix different = generateMockScholix(true,4, false,5);

        assertNotEquals(different,right);
        assertNotEquals(different,left);
        assertNotEquals(left.hashCode(), different.hashCode());
        assertNotEquals(right.hashCode(), different.hashCode());


    }


    @Test
    public void testCompareScholixRelation() {
        final ScholixRelationship left = generateMockScholixRelationships(true);
        final ScholixRelationship right = generateMockScholixRelationships(false);
        assertEquals(left, right);
        assertEquals(left.hashCode(), right.hashCode());

    }




}
