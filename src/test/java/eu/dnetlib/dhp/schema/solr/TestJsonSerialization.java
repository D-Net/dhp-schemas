package eu.dnetlib.dhp.schema.solr;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestJsonSerialization {

    private static ObjectMapper MAPPER = new ObjectMapper();

    @Test
    void testSerialiseJsonPayload() throws IOException {
        Result r = new Result();

        r.setIsGreen(true);
        r.setIsInDiamondJournal(false);

        final String json = MAPPER.writeValueAsString(r);

        assertNotNull(json);

        System.out.println("json = " + json);

        Result r1 = MAPPER.readValue(json, Result.class);

        assertTrue(r1.getIsGreen());
        assertFalse(r1.getIsInDiamondJournal());
    }
}
