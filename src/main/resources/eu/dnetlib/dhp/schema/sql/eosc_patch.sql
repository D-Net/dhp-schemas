-- Table to extend the datasource properties according to the eosc model
CREATE TABLE dsm_datasources_eosc(
	id               varchar(255) references dsm_datasources(id),
	jurisdiction     text,
	thematic         boolean,
	knowledge_graph  boolean,
	content_policies text[],
	_dnet_resource_identifier_ varchar(2048) default ((('temp_'::text || md5((clock_timestamp())::text)) || '_'::text) || md5((random())::text))
);
