
package eu.dnetlib.dhp.schema.action;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.dnetlib.dhp.schema.oaf.Oaf;

public class AtomicActionDeserializer<T extends Oaf> extends JsonDeserializer<AtomicAction<T>> {

	@Override
	@SuppressWarnings("unchecked")
	public AtomicAction<T> deserialize(JsonParser jp, DeserializationContext ctxt)
		throws IOException {
		JsonNode node = jp.getCodec().readTree(jp);
		String classTag = node.get("clazz").asText();
		JsonNode payload = node.get("payload");
		ObjectMapper mapper = new ObjectMapper();

		try {
			final Class<T> clazz = (Class<T>) Class.forName(classTag);
			final T oaf = mapper.readValue(payload.toString(), clazz);
			return new AtomicAction<>(clazz, oaf);
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		}
	}
}
