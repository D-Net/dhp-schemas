package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

/**
 * Used to refer to the Article Processing Charge information. It contains two parameters: -
 * currency of type String to store the currency of the APC - amount of type String to stores the charged amount
 */
public class APC implements Serializable {

    private static final long serialVersionUID = 8324447899738993669L;

    private String currency;

    private String amount;

    public static APC newInstance(String currency, String amount) {
        APC apc = new APC();
        apc.setCurrency(currency);
        apc.setAmount(amount);
        return apc;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}