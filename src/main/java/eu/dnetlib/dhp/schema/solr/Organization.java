package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Organization implements Serializable {

    private static final long serialVersionUID = -4287924905285274034L;

    private String legalshortname;

    private String legalname;

    private List<String> alternativeNames;

    private String websiteurl;

    private String logourl;

    private String eclegalbody;

    private String eclegalperson;

    private String ecnonprofit;

    private String ecresearchorganization;

    private String echighereducation;

    private String ecinternationalorganizationeurinterests;

    private String ecinternationalorganization;

    private String ecenterprise;

    private String ecsmevalidated;

    private String ecnutscode;

    private CodeLabel country;

    public String getLegalshortname() {
        return legalshortname;
    }

    public void setLegalshortname(String legalshortname) {
        this.legalshortname = legalshortname;
    }

    public String getLegalname() {
        return legalname;
    }

    public void setLegalname(String legalname) {
        this.legalname = legalname;
    }

    public List<String> getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(List<String> alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getLogourl() {
        return logourl;
    }

    public void setLogourl(String logourl) {
        this.logourl = logourl;
    }

    public String getEclegalbody() {
        return eclegalbody;
    }

    public void setEclegalbody(String eclegalbody) {
        this.eclegalbody = eclegalbody;
    }

    public String getEclegalperson() {
        return eclegalperson;
    }

    public void setEclegalperson(String eclegalperson) {
        this.eclegalperson = eclegalperson;
    }

    public String getEcnonprofit() {
        return ecnonprofit;
    }

    public void setEcnonprofit(String ecnonprofit) {
        this.ecnonprofit = ecnonprofit;
    }

    public String getEcresearchorganization() {
        return ecresearchorganization;
    }

    public void setEcresearchorganization(String ecresearchorganization) {
        this.ecresearchorganization = ecresearchorganization;
    }

    public String getEchighereducation() {
        return echighereducation;
    }

    public void setEchighereducation(String echighereducation) {
        this.echighereducation = echighereducation;
    }

    public String getEcinternationalorganizationeurinterests() {
        return ecinternationalorganizationeurinterests;
    }

    public void setEcinternationalorganizationeurinterests(String ecinternationalorganizationeurinterests) {
        this.ecinternationalorganizationeurinterests = ecinternationalorganizationeurinterests;
    }

    public String getEcinternationalorganization() {
        return ecinternationalorganization;
    }

    public void setEcinternationalorganization(String ecinternationalorganization) {
        this.ecinternationalorganization = ecinternationalorganization;
    }

    public String getEcenterprise() {
        return ecenterprise;
    }

    public void setEcenterprise(String ecenterprise) {
        this.ecenterprise = ecenterprise;
    }

    public String getEcsmevalidated() {
        return ecsmevalidated;
    }

    public void setEcsmevalidated(String ecsmevalidated) {
        this.ecsmevalidated = ecsmevalidated;
    }

    public String getEcnutscode() {
        return ecnutscode;
    }

    public void setEcnutscode(String ecnutscode) {
        this.ecnutscode = ecnutscode;
    }

    public CodeLabel getCountry() {
        return country;
    }

    public void setCountry(CodeLabel country) {
        this.country = country;
    }
}
