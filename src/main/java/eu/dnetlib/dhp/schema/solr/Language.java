package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class Language implements Serializable {

    private static final long serialVersionUID = -8308628905005193974L;

    /**
     * alpha-3/ISO 639-2 code of the language
     */
    private String code; // the classid in the Qualifier

    /**
     * Language label in English
     */
    private String label; // the classname in the Qualifier

    public static Language newInstance(String code, String label) {
        Language lang = new Language();
        lang.setCode(code);
        lang.setLabel(label);
        return lang;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}