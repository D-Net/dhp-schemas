package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Funder implements Serializable {

    private static final long serialVersionUID = -8008317145200052214L;

    private String id;
    private String shortname;
    private String name;
    private Country jurisdiction;
    private List<Pid> pid;

    public static Funder newInstance(String id, String shortname, String name, Country jurisdiction, List<Pid> pid) {
        Funder funder = new Funder();
        funder.setId(id);
        funder.setShortname(shortname);
        funder.setName(name);
        funder.setJurisdiction(jurisdiction);
        funder.setPid(pid);
        return funder;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Country getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Country jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public List<Pid> getPid() {
        return pid;
    }

    public void setPid(List<Pid> pid) {
        this.pid = pid;
    }
}
