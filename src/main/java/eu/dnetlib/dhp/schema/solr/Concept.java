package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class Concept implements Serializable {

    private static final long serialVersionUID = -2309379406128410374L;

    private String id;
    private String label;

    public static Concept newInstance(String id, String label) {
        Concept concept = new Concept();
        concept.setId(id);
        concept.setLabel(label);
        return concept;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
