package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class CodeLabel implements Serializable {

    private static final long serialVersionUID = 8182033641583483845L;

    private String code;

    private String label;

    public static CodeLabel newInstance(String code, String label) {
        CodeLabel cl = new CodeLabel();
        cl.setCode(code);
        cl.setLabel(label);
        return cl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
