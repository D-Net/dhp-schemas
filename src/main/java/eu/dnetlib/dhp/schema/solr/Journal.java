package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class Journal implements Serializable {

    private static final long serialVersionUID = -103050431178898773L;

    /**
     * Name of the journal or conference
     */
    private String name;

    /**
     * The issn
     */
    private String issnPrinted;

    /**
     * The e-issn
     */
    private String issnOnline;

    /**
     * The kinking issn
     */
    private String issnLinking;

    /**
     * Start page
     */
    private String sp;

    /**
     * End page
     */
    private String ep;

    /**
     * Journal issue number
     */
    private String iss;

    /**
     * Volume
     */
    private String vol;

    /**
     * Edition of the journal or conference proceeding
     */
    private String edition;

    private String conferenceplace;

    private String conferencedate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIssnPrinted() {
        return issnPrinted;
    }

    public void setIssnPrinted(String issnPrinted) {
        this.issnPrinted = issnPrinted;
    }

    public String getIssnOnline() {
        return issnOnline;
    }

    public void setIssnOnline(String issnOnline) {
        this.issnOnline = issnOnline;
    }

    public String getIssnLinking() {
        return issnLinking;
    }

    public void setIssnLinking(String issnLinking) {
        this.issnLinking = issnLinking;
    }

    public String getSp() {
        return sp;
    }

    public void setSp(String sp) {
        this.sp = sp;
    }

    public String getEp() {
        return ep;
    }

    public void setEp(String ep) {
        this.ep = ep;
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public String getVol() {
        return vol;
    }

    public void setVol(String vol) {
        this.vol = vol;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getConferenceplace() {
        return conferenceplace;
    }

    public void setConferenceplace(String conferenceplace) {
        this.conferenceplace = conferenceplace;
    }

    public String getConferencedate() {
        return conferencedate;
    }

    public void setConferencedate(String conferencedate) {
        this.conferencedate = conferencedate;
    }
}
