package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class SolrRecordHeader implements Serializable {

    private static final long serialVersionUID = -6052397109220149426L;

    public enum Status { UNDER_CURATION }

    /**
     * The OpenAIRE identifiers for this record
     */
    private String id;

    /**
     * Identifiers of the record at the original sources
     */
    private List<String> originalId;

    private RecordType recordType;

    private Status status = null;

    private Boolean deletedbyinference;

    public static SolrRecordHeader newInstance(String id, List<String> originalId, RecordType recordType, Boolean deletedbyinference) {
        return newInstance(id, originalId, recordType, null, deletedbyinference);
    }

    public static SolrRecordHeader newInstance(String id, List<String> originalId, RecordType recordType, Status status, Boolean deletedbyinference) {
        SolrRecordHeader header = new SolrRecordHeader();
        header.setId(id);
        header.setOriginalId(originalId);
        header.setRecordType(recordType);
        header.setStatus(status);
        header.setDeletedbyinference(deletedbyinference);
        return header;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getOriginalId() {
        return originalId;
    }

    public void setOriginalId(List<String> originalId) {
        this.originalId = originalId;
    }

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public Boolean getDeletedbyinference() {
        return deletedbyinference;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setDeletedbyinference(Boolean deletedbyinference) {
        this.deletedbyinference = deletedbyinference;
    }
}
