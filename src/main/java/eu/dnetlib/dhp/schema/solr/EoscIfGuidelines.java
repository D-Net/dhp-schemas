package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

/**
 * Describes a reference to the EOSC Interoperability Framework (IF) Guidelines
 */
public class EoscIfGuidelines implements Serializable {

    private static final long serialVersionUID = -6460491699309783829L;

    /**
     * EOSC-IF local code. Later on it could be populated with a PID (e.g. DOI), but for the time being we stick to
     * a more loose definition.
     */
    private String code;

    /**
     * EOSC-IF label
     */
    private String label;

    /**
     * EOSC-IF url
     */
    private String url;

    /**
     * EOSC-IF semantic relation (e.g. compliesWith). Values shall be controlled by a dedicated vocabulary.
     */
    private String semanticRelation;

    public static EoscIfGuidelines newInstance(String code, String label, String url, String semanticRelation) {
        EoscIfGuidelines e = new EoscIfGuidelines();
        e.setCode(code);
        e.setLabel(label);
        e.setUrl(url);
        e.setSemanticRelation(semanticRelation);
        return e;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSemanticRelation() {
        return semanticRelation;
    }

    public void setSemanticRelation(String semanticRelation) {
        this.semanticRelation = semanticRelation;
    }

}
