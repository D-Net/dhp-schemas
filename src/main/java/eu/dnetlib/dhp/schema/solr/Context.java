package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Context implements Serializable {

    private String id;
    private String label;
    private String type;
    private List<Category> category = new ArrayList<>();

    public static Context newInstance(String id, String label, String type, List<Category> category) {
        Context context = new Context();
        context.setId(id);
        context.setLabel(label);
        context.setType(type);
        context.setCategory(category);
        return context;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }
}
