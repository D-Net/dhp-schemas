package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Datasource implements Serializable {

    private static final long serialVersionUID = -5817484869244344681L;
    private CodeLabel datasourcetype;

    private CodeLabel datasourcetypeui;

    private CodeLabel eosctype; // Data Source | Service

    private CodeLabel eoscdatasourcetype;

    private CodeLabel openairecompatibility;

    private String officialname;

    private String englishname;

    private String websiteurl;

    private String logourl;

    private String contactemail;

    private String namespaceprefix;

    private String latitude;

    private String longitude;

    private String dateofvalidation;

    private String description;

    private List<Subject> subjects;

    private String odnumberofitems;

    private String odnumberofitemsdate;

    private String odpolicies;

    private List<String> odlanguages;

    private List<String> languages;

    private List<String> odcontenttypes;

    private List<String> accessinfopackage;

    // re3data fields
    private String releasestartdate;

    private String releaseenddate;

    private String missionstatementurl;

    private Boolean dataprovider;

    private Boolean serviceprovider;

    // {open, restricted or closed}
    private String databaseaccesstype;

    // {open, restricted or closed}
    private String datauploadtype;

    // {feeRequired, registration, other}
    private String databaseaccessrestriction;

    // {feeRequired, registration, other}
    private String datauploadrestriction;

    private Boolean versioning;

    private Boolean versioncontrol;

    private String citationguidelineurl;

    private String pidsystems;

    private String certificates;

    private List<CodeLabel> policies;

    private Journal journal;

    // New field for EOSC
    private List<String> researchentitytypes;

    // New field for EOSC
    private List<String> providedproducttypes;

    // New field for EOSC
    private CodeLabel jurisdiction;

    // New field for EOSC
    private Boolean thematic;

    // New field for EOSC
    private List<CodeLabel> contentpolicies;

    private String submissionpolicyurl;

    private String preservationpolicyurl;

    private List<String> researchproductaccesspolicies;

    private List<String> researchproductmetadataaccesspolicies;

    private Boolean consenttermsofuse;

    private Boolean fulltextdownload;

    private String consenttermsofusedate;

    private String lastconsenttermsofusedate;

    public CodeLabel getDatasourcetype() {
        return datasourcetype;
    }

    public void setDatasourcetype(CodeLabel datasourcetype) {
        this.datasourcetype = datasourcetype;
    }

    public CodeLabel getDatasourcetypeui() {
        return datasourcetypeui;
    }

    public void setDatasourcetypeui(CodeLabel datasourcetypeui) {
        this.datasourcetypeui = datasourcetypeui;
    }

    public CodeLabel getEosctype() {
        return eosctype;
    }

    public void setEosctype(CodeLabel eosctype) {
        this.eosctype = eosctype;
    }

    public CodeLabel getEoscdatasourcetype() {
        return eoscdatasourcetype;
    }

    public void setEoscdatasourcetype(CodeLabel eoscdatasourcetype) {
        this.eoscdatasourcetype = eoscdatasourcetype;
    }

    public CodeLabel getOpenairecompatibility() {
        return openairecompatibility;
    }

    public void setOpenairecompatibility(CodeLabel openairecompatibility) {
        this.openairecompatibility = openairecompatibility;
    }

    public String getOfficialname() {
        return officialname;
    }

    public void setOfficialname(String officialname) {
        this.officialname = officialname;
    }

    public String getEnglishname() {
        return englishname;
    }

    public void setEnglishname(String englishname) {
        this.englishname = englishname;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getLogourl() {
        return logourl;
    }

    public void setLogourl(String logourl) {
        this.logourl = logourl;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public String getNamespaceprefix() {
        return namespaceprefix;
    }

    public void setNamespaceprefix(String namespaceprefix) {
        this.namespaceprefix = namespaceprefix;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDateofvalidation() {
        return dateofvalidation;
    }

    public void setDateofvalidation(String dateofvalidation) {
        this.dateofvalidation = dateofvalidation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public String getOdnumberofitems() {
        return odnumberofitems;
    }

    public void setOdnumberofitems(String odnumberofitems) {
        this.odnumberofitems = odnumberofitems;
    }

    public String getOdnumberofitemsdate() {
        return odnumberofitemsdate;
    }

    public void setOdnumberofitemsdate(String odnumberofitemsdate) {
        this.odnumberofitemsdate = odnumberofitemsdate;
    }

    public String getOdpolicies() {
        return odpolicies;
    }

    public void setOdpolicies(String odpolicies) {
        this.odpolicies = odpolicies;
    }

    public List<String> getOdlanguages() {
        return odlanguages;
    }

    public void setOdlanguages(List<String> odlanguages) {
        this.odlanguages = odlanguages;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public List<String> getOdcontenttypes() {
        return odcontenttypes;
    }

    public void setOdcontenttypes(List<String> odcontenttypes) {
        this.odcontenttypes = odcontenttypes;
    }

    public List<String> getAccessinfopackage() {
        return accessinfopackage;
    }

    public void setAccessinfopackage(List<String> accessinfopackage) {
        this.accessinfopackage = accessinfopackage;
    }

    public String getReleasestartdate() {
        return releasestartdate;
    }

    public void setReleasestartdate(String releasestartdate) {
        this.releasestartdate = releasestartdate;
    }

    public String getReleaseenddate() {
        return releaseenddate;
    }

    public void setReleaseenddate(String releaseenddate) {
        this.releaseenddate = releaseenddate;
    }

    public String getMissionstatementurl() {
        return missionstatementurl;
    }

    public void setMissionstatementurl(String missionstatementurl) {
        this.missionstatementurl = missionstatementurl;
    }

    public Boolean getDataprovider() {
        return dataprovider;
    }

    public void setDataprovider(Boolean dataprovider) {
        this.dataprovider = dataprovider;
    }

    public Boolean getServiceprovider() {
        return serviceprovider;
    }

    public void setServiceprovider(Boolean serviceprovider) {
        this.serviceprovider = serviceprovider;
    }

    public String getDatabaseaccesstype() {
        return databaseaccesstype;
    }

    public void setDatabaseaccesstype(String databaseaccesstype) {
        this.databaseaccesstype = databaseaccesstype;
    }

    public String getDatauploadtype() {
        return datauploadtype;
    }

    public void setDatauploadtype(String datauploadtype) {
        this.datauploadtype = datauploadtype;
    }

    public String getDatabaseaccessrestriction() {
        return databaseaccessrestriction;
    }

    public void setDatabaseaccessrestriction(String databaseaccessrestriction) {
        this.databaseaccessrestriction = databaseaccessrestriction;
    }

    public String getDatauploadrestriction() {
        return datauploadrestriction;
    }

    public void setDatauploadrestriction(String datauploadrestriction) {
        this.datauploadrestriction = datauploadrestriction;
    }

    public Boolean getVersioning() {
        return versioning;
    }

    public void setVersioning(Boolean versioning) {
        this.versioning = versioning;
    }

    public Boolean getVersioncontrol() {
        return versioncontrol;
    }

    public void setVersioncontrol(Boolean versioncontrol) {
        this.versioncontrol = versioncontrol;
    }

    public String getCitationguidelineurl() {
        return citationguidelineurl;
    }

    public void setCitationguidelineurl(String citationguidelineurl) {
        this.citationguidelineurl = citationguidelineurl;
    }

    public String getPidsystems() {
        return pidsystems;
    }

    public void setPidsystems(String pidsystems) {
        this.pidsystems = pidsystems;
    }

    public String getCertificates() {
        return certificates;
    }

    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    public List<CodeLabel> getPolicies() {
        return policies;
    }

    public void setPolicies(List<CodeLabel> policies) {
        this.policies = policies;
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public List<String> getResearchentitytypes() {
        return researchentitytypes;
    }

    public void setResearchentitytypes(List<String> researchentitytypes) {
        this.researchentitytypes = researchentitytypes;
    }

    public List<String> getProvidedproducttypes() {
        return providedproducttypes;
    }

    public void setProvidedproducttypes(List<String> providedproducttypes) {
        this.providedproducttypes = providedproducttypes;
    }

    public CodeLabel getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(CodeLabel jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public Boolean getThematic() {
        return thematic;
    }

    public void setThematic(Boolean thematic) {
        this.thematic = thematic;
    }

    public List<CodeLabel> getContentpolicies() {
        return contentpolicies;
    }

    public void setContentpolicies(List<CodeLabel> contentpolicies) {
        this.contentpolicies = contentpolicies;
    }

    public String getSubmissionpolicyurl() {
        return submissionpolicyurl;
    }

    public void setSubmissionpolicyurl(String submissionpolicyurl) {
        this.submissionpolicyurl = submissionpolicyurl;
    }

    public String getPreservationpolicyurl() {
        return preservationpolicyurl;
    }

    public void setPreservationpolicyurl(String preservationpolicyurl) {
        this.preservationpolicyurl = preservationpolicyurl;
    }

    public List<String> getResearchproductaccesspolicies() {
        return researchproductaccesspolicies;
    }

    public void setResearchproductaccesspolicies(List<String> researchproductaccesspolicies) {
        this.researchproductaccesspolicies = researchproductaccesspolicies;
    }

    public List<String> getResearchproductmetadataaccesspolicies() {
        return researchproductmetadataaccesspolicies;
    }

    public void setResearchproductmetadataaccesspolicies(List<String> researchproductmetadataaccesspolicies) {
        this.researchproductmetadataaccesspolicies = researchproductmetadataaccesspolicies;
    }

    public Boolean getConsenttermsofuse() {
        return consenttermsofuse;
    }

    public void setConsenttermsofuse(Boolean consenttermsofuse) {
        this.consenttermsofuse = consenttermsofuse;
    }

    public Boolean getFulltextdownload() {
        return fulltextdownload;
    }

    public void setFulltextdownload(Boolean fulltextdownload) {
        this.fulltextdownload = fulltextdownload;
    }

    public String getConsenttermsofusedate() {
        return consenttermsofusedate;
    }

    public void setConsenttermsofusedate(String consenttermsofusedate) {
        this.consenttermsofusedate = consenttermsofusedate;
    }

    public String getLastconsenttermsofusedate() {
        return lastconsenttermsofusedate;
    }

    public void setLastconsenttermsofusedate(String lastconsenttermsofusedate) {
        this.lastconsenttermsofusedate = lastconsenttermsofusedate;
    }
}
