package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class Provenance implements Serializable {

    private static final long serialVersionUID = -474833313621479191L;

    private String dsId;

    private String dsName;

    public static Provenance newInstance(String dsId, String dsName) {
        Provenance p = new Provenance();
        p.setDsId(dsId);
        p.setDsName(dsName);
        return p;
    }

    public String getDsId() {
        return dsId;
    }

    public void setDsId(String dsId) {
        this.dsId = dsId;
    }

    public String getDsName() {
        return dsName;
    }

    public void setDsName(String dsName) {
        this.dsName = dsName;
    }
}
