package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class Pid implements Serializable {

    private static final long serialVersionUID = -943684282582228545L;

    private String value;

    private String typeCode;

    private String typeLabel;

    public static Pid newInstance(String value, String typeCode, String typeLabel) {
        Pid p = new Pid();
        p.setValue(value);
        p.setTypeCode(typeCode);
        p.setTypeLabel(typeLabel);
        return p;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }
}
