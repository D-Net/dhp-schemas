package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Instance implements Serializable {

    private static final long serialVersionUID = 7440831757124257169L;

    private String license;

    private AccessRight accessright;

    private String instancetype;

    private Provenance hostedby;

    private List<String> url;

    // other research products specifc
    private String distributionlocation;

    private Provenance collectedfrom;

    private List<Pid> pid;

    private List<Pid> alternateIdentifier;

    private String dateofacceptance;

    // ( article | book ) processing charges. Defined here to cope with possible wrongly typed
    // results
    private APC processingcharges;

    private String refereed; // peer-review status

    private List<Measure> measures;

    /**
     * Direct fulltext URL.
     */
    private String fulltext;

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public AccessRight getAccessright() {
        return accessright;
    }

    public void setAccessright(AccessRight accessright) {
        this.accessright = accessright;
    }

    public String getInstancetype() {
        return instancetype;
    }

    public void setInstancetype(String instancetype) {
        this.instancetype = instancetype;
    }

    public Provenance getHostedby() {
        return hostedby;
    }

    public void setHostedby(Provenance hostedby) {
        this.hostedby = hostedby;
    }

    public List<String> getUrl() {
        return url;
    }

    public void setUrl(List<String> url) {
        this.url = url;
    }

    public String getDistributionlocation() {
        return distributionlocation;
    }

    public void setDistributionlocation(String distributionlocation) {
        this.distributionlocation = distributionlocation;
    }

    public Provenance getCollectedfrom() {
        return collectedfrom;
    }

    public void setCollectedfrom(Provenance collectedfrom) {
        this.collectedfrom = collectedfrom;
    }

    public List<Pid> getPid() {
        return pid;
    }

    public void setPid(List<Pid> pid) {
        this.pid = pid;
    }

    public List<Pid> getAlternateIdentifier() {
        return alternateIdentifier;
    }

    public void setAlternateIdentifier(List<Pid> alternateIdentifier) {
        this.alternateIdentifier = alternateIdentifier;
    }

    public String getDateofacceptance() {
        return dateofacceptance;
    }

    public void setDateofacceptance(String dateofacceptance) {
        this.dateofacceptance = dateofacceptance;
    }

    public APC getProcessingcharges() {
        return processingcharges;
    }

    public void setProcessingcharges(APC processingcharges) {
        this.processingcharges = processingcharges;
    }

    public String getRefereed() {
        return refereed;
    }

    public void setRefereed(String refereed) {
        this.refereed = refereed;
    }

    public List<Measure> getMeasures() {
        return measures;
    }

    public void setMeasures(List<Measure> measures) {
        this.measures = measures;
    }

    public String getFulltext() {
        return fulltext;
    }

    public void setFulltext(String fulltext) {
        this.fulltext = fulltext;
    }
}
