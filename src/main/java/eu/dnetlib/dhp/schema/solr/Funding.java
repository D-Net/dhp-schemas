package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class Funding implements Serializable {

    private static final long serialVersionUID = 1811734787904369695L;

    private Funder funder;
    private FundingLevel level0;
    private FundingLevel level1;
    private FundingLevel level2;

    public static Funding newInstance(Funder funder, FundingLevel level0) {
        return newInstance(funder, level0, null, null);
    }

    public static Funding newInstance(Funder funder, FundingLevel level0, FundingLevel level1) {
        return newInstance(funder, level0, level1, null);
    }

    public static Funding newInstance(Funder funder, FundingLevel level0, FundingLevel level1, FundingLevel level2) {
        Funding funding = new Funding();
        funding.setFunder(funder);
        funding.setLevel0(level0);
        funding.setLevel1(level1);
        funding.setLevel2(level2);
        return funding;
    }

    public Funder getFunder() {
        return funder;
    }

    public void setFunder(Funder funder) {
        this.funder = funder;
    }

    public FundingLevel getLevel0() {
        return level0;
    }

    public void setLevel0(FundingLevel level0) {
        this.level0 = level0;
    }

    public FundingLevel getLevel1() {
        return level1;
    }

    public void setLevel1(FundingLevel level1) {
        this.level1 = level1;
    }

    public FundingLevel getLevel2() {
        return level2;
    }

    public void setLevel2(FundingLevel level2) {
        this.level2 = level2;
    }
}
