package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Person implements Serializable {

    private static final long serialVersionUID = 3923041787040187202L;

    private String givenName;

    private String familyName;

    private List<String> alternativeNames;

    private String biography;

    private List<PersonTopic> subject;

    /**
     * The Measures.
     */
    private List<Measure> indicator;

    /**
     * The Context.
     */
    private List<Context> context;

    private Boolean consent;

    public static Person newInstance(String givenName, String familyName, List<String> alternativeNames, String biography, List<PersonTopic> subject, List<Measure> indicator, List<Context> context, Boolean consent) {
        final Person p = new Person();

        p.setGivenName(givenName);
        p.setFamilyName(familyName);
        p.setAlternativeNames(alternativeNames);
        p.setBiography(biography);
        p.setSubject(subject);
        p.setIndicator(indicator);
        p.setContext(context);
        p.setConsent(consent);

        return p;
    }

    public Person() {
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public List<String> getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(List<String> alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public List<PersonTopic> getSubject() {
        return subject;
    }

    public void setSubject(List<PersonTopic> subject) {
        this.subject = subject;
    }

    public List<Measure> getIndicator() {
        return indicator;
    }

    public void setIndicator(List<Measure> indicator) {
        this.indicator = indicator;
    }

    public List<Context> getContext() {
        return context;
    }

    public void setContext(List<Context> context) {
        this.context = context;
    }

    public Boolean getConsent() {
        return consent;
    }

    public void setConsent(Boolean consent) {
        this.consent = consent;
    }
}
