package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class RelatedRecordHeader implements Serializable {

    private static final long serialVersionUID = -1491635071695452382L;

    private String relationType;

    private String relationClass;

    private String relatedIdentifier;

    private RecordType relatedRecordType;

    private String relationProvenance;

    private String trust;

    public static RelatedRecordHeader newInstance(String relationType, String relationClass, String relatedIdentifier, RecordType relatedRecordType, String relationProvenance, String trust) {
        RelatedRecordHeader header = new RelatedRecordHeader();
        header.setRelationType(relationType);
        header.setRelationClass(relationClass);
        header.setRelatedIdentifier(relatedIdentifier);
        header.setRelatedRecordType(relatedRecordType);
        header.setRelationProvenance(relationProvenance);
        header.setTrust(trust);
        return header;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getRelationClass() {
        return relationClass;
    }

    public void setRelationClass(String relationClass) {
        this.relationClass = relationClass;
    }

    public String getRelatedIdentifier() {
        return relatedIdentifier;
    }

    public void setRelatedIdentifier(String relatedIdentifier) {
        this.relatedIdentifier = relatedIdentifier;
    }

    public RecordType getRelatedRecordType() {
        return relatedRecordType;
    }

    public void setRelatedRecordType(RecordType relatedRecordType) {
        this.relatedRecordType = relatedRecordType;
    }

    public String getRelationProvenance() {
        return relationProvenance;
    }

    public void setRelationProvenance(String relationProvenance) {
        this.relationProvenance = relationProvenance;
    }

    public String getTrust() {
        return trust;
    }

    public void setTrust(String trust) {
        this.trust = trust;
    }
}
