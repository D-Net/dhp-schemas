package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Author implements Serializable {

    private static final long serialVersionUID = 5482727671039062564L;

    private String fullname;

    private String name;

    private String surname;

    private Integer rank;

    /**
     * The author's persistent identifiers
     */
    private List<Pid> pid;

    public static Author newInstance(String fullname, String name, String surname, int rank, List<Pid> pid) {
        Author a = new Author();
        a.setFullname(fullname);
        a.setName(name);
        a.setSurname(surname);
        a.setRank(rank);
        a.setPid(pid);
        return a;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public List<Pid> getPid() {
        return pid;
    }

    public void setPid(List<Pid> pid) {
        this.pid = pid;
    }
}