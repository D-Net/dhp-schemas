package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable {

    private static final long serialVersionUID = -9121038426985022737L;

    /**
     * Type of the result: one of 'publication', 'dataset', 'software', 'other' (see also https://api.openaire.eu/vocabularies/dnet:result_typologies)
     * This field is deprecated. See eu.dnetlib.dhp.schema.solr.SolrRecordHeader.RecordType
     */
    @Deprecated
    private String resulttype;

    /**
     * Authors of the result
     */
    private List<Author> author;

    /**
     * The Subject.
     */
    private List<Subject> subject;

    /**
     * The result language
     */
    private Language language;

    /**
     * The list of countries associated to this result
     */
    private List<Country> country;

    /**
     * A name or title by which a scientific result is known. May be the title of a publication, of a dataset or the name of a piece of software.
     */
    private String maintitle;

    /**
     * Explanatory or alternative names by which a scientific result is known.
     */
    private List<String> otherTitles;

    private List<String> description;

    /**
     * Main date of the research product: typically the publication or issued date. In case of a research result with
     * different versions with different dates, the date of the result is selected as the most frequent well-formatted date.
     * If not available, then the most recent and complete date among those that are well-formatted.
     * For statistics, the year is extracted and the result is counted only among the result of that year.
     * Example:
     *  Pre-print date: 2019-02-03,
     *  Article date provided by repository: 2020-02,
     *  Article date provided by Crossref: 2020,
     *  OpenAIRE will set as date 2019-02-03, because it’s the most recent among the complete and well-formed dates.
     *  If then the repository updates the metadata and set a complete date (e.g. 2020-02-12), then this will be the new
     *  date for the result because it becomes the most recent most complete date.
     *  However, if OpenAIRE then collects the pre-print from another repository with date 2019-02-03, then this will be
     *  the “winning date” because it becomes the most frequent well-formatted date.
     */
    private String publicationdate; // dateofacceptance

    /**
     * The name of the entity that holds, archives, publishes prints, distributes, releases, issues, or produces the resource.
     */
    private String publisher;

    /**
     * Date when the embargo ends and this result turns Open Access
     */
    private String embargoenddate;

    /**
     *  See definition of Dublin Core field dc:source
     */
    private List<String> source;

    private List<String> format;

    /**
     * Contributors for the result
     */
    private List<String> contributor;

    private List<String> coverage;

    /**
     * The openest of the access rights of this result.
     */
    private BestAccessRight bestaccessright;

    /**
     * The direct link to the full-text as collected from the data source
     */
    private List<String> fulltext;

    /**
     * Journal has information about the conference or journal where the result has been presented or published
     */
    private Journal journal;

    /**
     * Only for results with type 'software': URL to the software documentation
     */
    private List<String> documentationUrl; // software

    /**
     * Only for results with type 'software': the URL to the repository with the source code
     */
    private String codeRepositoryUrl; // software

    /**
     * Only for results with type 'software': the programming language
     */
    private String programmingLanguage; // software

    /**
     * Only for results with type 'software': Information on the person responsible for providing further information regarding the resource
     */
    private List<String> contactperson; // orp

    /**
     * Only for results with type 'software': Information on the group responsible for providing further information regarding the resource
     */
    private List<String> contactgroup; // orp

    /**
     * Only for results with type 'other': tool useful for the interpretation and/or re-used of the research product
     */
    private List<String> tool; // orp

    /**
     * Only for results with type 'dataset': the declared size of the dataset
     */
    private String size; // dataset

    /**
     * Version of the result
     */
    private String version; // dataset

    /**
     * EOSC Interoperability Framework Guidelines
     */
    private List<EoscIfGuidelines> eoscifguidelines;

    private Boolean isGreen;

    private OpenAccessColor openAccessColor;

    private Boolean isInDiamondJournal;

    private Boolean publiclyFunded;

    private String transformativeAgreement;

    private List<ExternalReference> externalReference;

    private String bestinstancetype;

    /**
     * Each instance is one specific materialisation or version of the result. For example, you can have one result with
     * three instance: one is the pre-print, one is the post-print, one is te published version
     */
    private List<Instance> instance;

    /**
     * This field is deprecated. See eu.dnetlib.dhp.schema.solr.SolrRecordHeader.RecordType
     * @return resulttype
     */
    @Deprecated
    public String getResulttype() {
        return resulttype;
    }

    @Deprecated
    public void setResulttype(String resulttype) {
        this.resulttype = resulttype;
    }

    public List<Author> getAuthor() {
        return author;
    }

    public void setAuthor(List<Author> author) {
        this.author = author;
    }

    public List<Subject> getSubject() {
        return subject;
    }

    public void setSubject(List<Subject> subject) {
        this.subject = subject;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<Country> getCountry() {
        return country;
    }

    public void setCountry(List<Country> country) {
        this.country = country;
    }

    public String getMaintitle() {
        return maintitle;
    }

    public void setMaintitle(String maintitle) {
        this.maintitle = maintitle;
    }

    public List<String> getOtherTitles() {
        return otherTitles;
    }

    public void setOtherTitles(List<String> otherTitles) {
        this.otherTitles = otherTitles;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public String getPublicationdate() {
        return publicationdate;
    }

    public void setPublicationdate(String publicationdate) {
        this.publicationdate = publicationdate;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getEmbargoenddate() {
        return embargoenddate;
    }

    public void setEmbargoenddate(String embargoenddate) {
        this.embargoenddate = embargoenddate;
    }

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public List<String> getFormat() {
        return format;
    }

    public void setFormat(List<String> format) {
        this.format = format;
    }

    public List<String> getContributor() {
        return contributor;
    }

    public void setContributor(List<String> contributor) {
        this.contributor = contributor;
    }

    public List<String> getCoverage() {
        return coverage;
    }

    public void setCoverage(List<String> coverage) {
        this.coverage = coverage;
    }

    public BestAccessRight getBestaccessright() {
        return bestaccessright;
    }

    public void setBestaccessright(BestAccessRight bestaccessright) {
        this.bestaccessright = bestaccessright;
    }

    public List<String> getFulltext() {
        return fulltext;
    }

    public void setFulltext(List<String> fulltext) {
        this.fulltext = fulltext;
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public List<String> getDocumentationUrl() {
        return documentationUrl;
    }

    public void setDocumentationUrl(List<String> documentationUrl) {
        this.documentationUrl = documentationUrl;
    }

    public String getCodeRepositoryUrl() {
        return codeRepositoryUrl;
    }

    public void setCodeRepositoryUrl(String codeRepositoryUrl) {
        this.codeRepositoryUrl = codeRepositoryUrl;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public List<String> getContactperson() {
        return contactperson;
    }

    public void setContactperson(List<String> contactperson) {
        this.contactperson = contactperson;
    }

    public List<String> getContactgroup() {
        return contactgroup;
    }

    public void setContactgroup(List<String> contactgroup) {
        this.contactgroup = contactgroup;
    }

    public List<String> getTool() {
        return tool;
    }

    public void setTool(List<String> tool) {
        this.tool = tool;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<EoscIfGuidelines> getEoscifguidelines() {
        return eoscifguidelines;
    }

    public void setEoscifguidelines(List<EoscIfGuidelines> eoscifguidelines) {
        this.eoscifguidelines = eoscifguidelines;
    }

    public Boolean getIsGreen() {
        return isGreen;
    }

    public void setIsGreen(Boolean green) {
        isGreen = green;
    }

    public OpenAccessColor getOpenAccessColor() {
        return openAccessColor;
    }

    public void setOpenAccessColor(OpenAccessColor openAccessColor) {
        this.openAccessColor = openAccessColor;
    }

    public Boolean getIsInDiamondJournal() {
        return isInDiamondJournal;
    }

    public void setIsInDiamondJournal(Boolean inDiamondJournal) {
        isInDiamondJournal = inDiamondJournal;
    }

    public Boolean getPubliclyFunded() {
        return publiclyFunded;
    }

    public void setPubliclyFunded(Boolean publiclyFunded) {
        this.publiclyFunded = publiclyFunded;
    }

    public String getTransformativeAgreement() {
        return transformativeAgreement;
    }

    public void setTransformativeAgreement(String transformativeAgreement) {
        this.transformativeAgreement = transformativeAgreement;
    }

    public List<ExternalReference> getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(List<ExternalReference> externalReference) {
        this.externalReference = externalReference;
    }

    public String getBestinstancetype() {
        return bestinstancetype;
    }

    public void setBestinstancetype(String bestinstancetype) {
        this.bestinstancetype = bestinstancetype;
    }

    public List<Instance> getInstance() {
        return instance;
    }

    public void setInstance(List<Instance> instance) {
        this.instance = instance;
    }
}
