package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

/**
 * BestAccessRight. Used to represent the result best access rights.
 */
public class BestAccessRight implements Serializable {

    private static final long serialVersionUID = 689431927147731065L;

    /**
     * AccessRight code
     */
    private String code; // the classid in the Qualifier

    /**
     * Label for the access mode
     */
    private String label; // the classname in the Qualifier

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static BestAccessRight newInstance(String code, String label) {
        BestAccessRight ar = new BestAccessRight();
        ar.code = code;
        ar.label = label;
        return ar;
    }
}