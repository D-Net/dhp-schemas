package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

/**
 * Represents the country associated to the generic entity. The value for this element correspond to: - code corresponds
 * to the classid of eu.dnetlib.dhp.schema.oaf.Country - label corresponds to the classname of
 * eu.dnetlib.dhp.schema.oaf.Country - provenance set only if the dataInfo associated to the Country of the result to be
 * dumped is not null. In this case : - provenance corresponds to dataInfo.provenanceaction.classid (to be modified with
 * datainfo.provenanceaction.classname) - trust corresponds to dataInfo.trust
 */
public class Country implements Serializable {

    private static final long serialVersionUID = 544249673120027949L;

    /**
     * ISO 3166-1 alpha-2 country code (i.e. IT)
     */
    private String code; // the classid in the Qualifier

    /**
     * The label for that code (i.e. Italy)
     */
    private String label; // the classname in the Qualifier

    public static Country newInstance(String code, String label) {
        Country c = new Country();
        c.setCode(code);
        c.setLabel(label);
        return c;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}