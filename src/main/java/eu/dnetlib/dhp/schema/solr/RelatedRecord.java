package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class RelatedRecord implements Serializable {

    private static final long serialVersionUID = -7251015928787319389L;

    private RelatedRecordHeader header;

    // common fields
    private String title;
    private String websiteurl; // datasource, organizations, projects
    private List<Pid> pid;
    private List<Provenance> collectedfrom;

    // results
    private List<String> author;
    private String description;
    private String dateofacceptance;
    private String publisher;
    private String codeRepositoryUrl;
    private String resulttype;
    private List<Instance> instances;

    // datasource
    private String officialname;
    private CodeLabel datasourcetype;
    private CodeLabel datasourcetypeui;
    private CodeLabel openairecompatibility;

    // organization
    private String legalname;
    private String legalshortname;
    private Country country;

    // project
    private String projectTitle;
    private String code;
    private String acronym;
    private CodeLabel contracttype;
    private Funding funding;
    private String validationDate;
    private String startDate;
    private String endDate;

    // person
    private String givenName;
    private String familyName;

    // relation properties
    // TODO: define it as an Enum
    private String personRoleInProject;

    /**
     * Represents the unordered list of start/end dates found in the affiliation (person-organization) links.
     */
    private List<CodeLabel> affiliationsTimeline;

    public RelatedRecordHeader getHeader() {
        return header;
    }

    public void setHeader(RelatedRecordHeader header) {
        this.header = header;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthor() {
        return author;
    }

    public void setAuthor(List<String> author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getDateofacceptance() {
        return dateofacceptance;
    }

    public void setDateofacceptance(String dateofacceptance) {
        this.dateofacceptance = dateofacceptance;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List<Pid> getPid() {
        return pid;
    }

    public void setPid(List<Pid> pid) {
        this.pid = pid;
    }

    public String getCodeRepositoryUrl() {
        return codeRepositoryUrl;
    }

    public void setCodeRepositoryUrl(String codeRepositoryUrl) {
        this.codeRepositoryUrl = codeRepositoryUrl;
    }

    public String getResulttype() {
        return resulttype;
    }

    public void setResulttype(String resulttype) {
        this.resulttype = resulttype;
    }

    public List<Provenance> getCollectedfrom() {
        return collectedfrom;
    }

    public void setCollectedfrom(List<Provenance> collectedfrom) {
        this.collectedfrom = collectedfrom;
    }

    public List<Instance> getInstances() {
        return instances;
    }

    public void setInstances(List<Instance> instances) {
        this.instances = instances;
    }

    public String getOfficialname() {
        return officialname;
    }

    public void setOfficialname(String officialname) {
        this.officialname = officialname;
    }

    public CodeLabel getDatasourcetype() {
        return datasourcetype;
    }

    public void setDatasourcetype(CodeLabel datasourcetype) {
        this.datasourcetype = datasourcetype;
    }

    public CodeLabel getDatasourcetypeui() {
        return datasourcetypeui;
    }

    public void setDatasourcetypeui(CodeLabel datasourcetypeui) {
        this.datasourcetypeui = datasourcetypeui;
    }

    public CodeLabel getOpenairecompatibility() {
        return openairecompatibility;
    }

    public void setOpenairecompatibility(CodeLabel openairecompatibility) {
        this.openairecompatibility = openairecompatibility;
    }

    public String getLegalname() {
        return legalname;
    }

    public void setLegalname(String legalname) {
        this.legalname = legalname;
    }

    public String getLegalshortname() {
        return legalshortname;
    }

    public void setLegalshortname(String legalshortname) {
        this.legalshortname = legalshortname;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public CodeLabel getContracttype() {
        return contracttype;
    }

    public void setContracttype(CodeLabel contracttype) {
        this.contracttype = contracttype;
    }

    public Funding getFunding() {
        return funding;
    }

    public void setFunding(Funding funding) {
        this.funding = funding;
    }

    public String getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getPersonRoleInProject() {
        return personRoleInProject;
    }

    public void setPersonRoleInProject(String personRoleInProject) {
        this.personRoleInProject = personRoleInProject;
    }

    public List<CodeLabel> getAffiliationsTimeline() {
        return affiliationsTimeline;
    }

    public void setAffiliationsTimeline(List<CodeLabel> affiliationsTimeline) {
        this.affiliationsTimeline = affiliationsTimeline;
    }
}
