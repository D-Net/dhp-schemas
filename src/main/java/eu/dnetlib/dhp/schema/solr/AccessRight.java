
package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

/**
 * This class models the access rights of research products.
 */
public class AccessRight implements Serializable {

	private static final long serialVersionUID = 7995566912611238604L;

	private String code;

	private String label;

	private OpenAccessRoute openAccessRoute;

	public static AccessRight newInstance(String code, String label, OpenAccessRoute openAccessRoute) {
		AccessRight accessRight = new AccessRight();
		accessRight.setCode(code);
		accessRight.setLabel(label);
		accessRight.setOpenAccessRoute(openAccessRoute);
		return accessRight;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public OpenAccessRoute getOpenAccessRoute() {
		return openAccessRoute;
	}

	public void setOpenAccessRoute(OpenAccessRoute openAccessRoute) {
		this.openAccessRoute = openAccessRoute;
	}


}
