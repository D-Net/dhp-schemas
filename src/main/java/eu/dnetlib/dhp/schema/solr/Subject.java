package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class Subject implements Serializable {

    private static final long serialVersionUID = -7242293435544930481L;

    private String value;

    private String typeCode;

    private String typeLabel;

    public static Subject newInstance(String value, String typeCode, String typeLabel) {
        Subject s = new Subject();
        s.setValue(value);
        s.setTypeCode(typeCode);
        s.setTypeLabel(typeLabel);
        return s;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }
}
