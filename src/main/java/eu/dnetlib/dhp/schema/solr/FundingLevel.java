package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

public class FundingLevel implements Serializable {

    private static final long serialVersionUID = 8270506546250477574L;

    private String id;

    private String description;

    private String name;

    public static FundingLevel newInstance(String id, String description, String name) {
        FundingLevel level = new FundingLevel();
        level.setId(id);
        level.setDescription(description);
        level.setName(name);
        return level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
