
package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

/**
 * The OpenAccess color meant to be used on the result level
 */
public enum OpenAccessColor implements Serializable {

	gold, hybrid, bronze

}
