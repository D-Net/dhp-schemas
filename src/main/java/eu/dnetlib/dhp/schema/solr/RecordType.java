package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum RecordType implements Serializable {
    publication,
    dataset,
    other,
    software,
    datasource,
    organization,
    project,
    person;

    @JsonCreator
    public static RecordType fromString(String s) {
        if (Objects.isNull(s)) {
            return null;
        }
        if ("otherresearchproduct".equalsIgnoreCase(s)) {
            return other;
        }
        return valueOf(s);
    }

}
