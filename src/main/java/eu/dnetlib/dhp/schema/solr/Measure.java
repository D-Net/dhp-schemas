package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Measure implements Serializable {

    private static final long serialVersionUID = 57939927121851504L;

    private String id;

    private List<CodeLabel> unit;

    public static Measure newInstance(String id, List<CodeLabel> unit) {
        Measure m = new Measure();
        m.setId(id);
        m.setUnit(unit);
        return m;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CodeLabel> getUnit() {
        return unit;
    }

    public void setUnit(List<CodeLabel> unit) {
        this.unit = unit;
    }
}
