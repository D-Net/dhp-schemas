package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class Project implements Serializable {

    private static final long serialVersionUID = -3712733565189222788L;

    private String websiteurl;

    private String code;
    private String acronym;
    private String title;
    private String startdate;
    private String enddate;
    private String callidentifier;
    private String keywords;
    private String duration;

    private String oamandatepublications;

    private String ecarticle29_3;

    private List<Subject> subjects;

    private CodeLabel contracttype;

    private String summary;

    private String currency;

    private Float totalcost;

    private Float fundedamount;

    private Funding funding;


    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getCallidentifier() {
        return callidentifier;
    }

    public void setCallidentifier(String callidentifier) {
        this.callidentifier = callidentifier;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getOamandatepublications() {
        return oamandatepublications;
    }

    public void setOamandatepublications(String oamandatepublications) {
        this.oamandatepublications = oamandatepublications;
    }

    public String getEcarticle29_3() {
        return ecarticle29_3;
    }

    public void setEcarticle29_3(String ecarticle29_3) {
        this.ecarticle29_3 = ecarticle29_3;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public CodeLabel getContracttype() {
        return contracttype;
    }

    public void setContracttype(CodeLabel contracttype) {
        this.contracttype = contracttype;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(Float totalcost) {
        this.totalcost = totalcost;
    }

    public Float getFundedamount() {
        return fundedamount;
    }

    public void setFundedamount(Float fundedamount) {
        this.fundedamount = fundedamount;
    }

    public Funding getFunding() {
        return funding;
    }

    public void setFunding(Funding funding) {
        this.funding = funding;
    }
}
