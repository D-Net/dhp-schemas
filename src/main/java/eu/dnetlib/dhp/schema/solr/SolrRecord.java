package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class SolrRecord implements Serializable {

    private static final long serialVersionUID = 2852901817696335899L;

    private SolrRecordHeader header;

    private List<Provenance> collectedfrom;

    /**
     * List of persistent identifiers
     */
    private List<Pid> pid;

    private List<Context> context;

    private List<Measure> measures;

    private Result result;

    private Datasource datasource;

    private Project project;

    private Organization organization;

    private Person person;

    private List<RelatedRecord> links;

    public SolrRecordHeader getHeader() {
        return header;
    }

    public void setHeader(SolrRecordHeader header) {
        this.header = header;
    }

    public List<Provenance> getCollectedfrom() {
        return collectedfrom;
    }

    public void setCollectedfrom(List<Provenance> collectedfrom) {
        this.collectedfrom = collectedfrom;
    }

    public List<Pid> getPid() {
        return pid;
    }

    public void setPid(List<Pid> pid) {
        this.pid = pid;
    }

    public List<Context> getContext() {
        return context;
    }

    public void setContext(List<Context> context) {
        this.context = context;
    }

    public List<Measure> getMeasures() {
        return measures;
    }

    public void setMeasures(List<Measure> measures) {
        this.measures = measures;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<RelatedRecord> getLinks() {
        return links;
    }

    public void setLinks(List<RelatedRecord> links) {
        this.links = links;
    }
}
