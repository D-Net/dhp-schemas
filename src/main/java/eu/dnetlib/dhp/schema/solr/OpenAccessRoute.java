
package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;

/**
 * This Enum models the OpenAccess status, currently including only the values from Unpaywall
 *
 * https://support.unpaywall.org/support/solutions/articles/44001777288-what-do-the-types-of-oa-status-green-gold-hybrid-and-bronze-mean-
 */
public enum OpenAccessRoute implements Serializable {

	gold, green, hybrid, bronze

}
