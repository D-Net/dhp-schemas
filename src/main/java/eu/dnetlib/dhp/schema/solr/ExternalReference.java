
package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.List;

public class ExternalReference implements Serializable {

	private static final long serialVersionUID = 781487309068059180L;

	// source
	private String sitename;

	// title
	private String label;

	// alternative labels
	private List<String> alternateLabel;

	// text()
	private String url;

	// type
	private CodeLabel qualifier;

	// site internal identifier
	private String refidentifier;

	// maps the oaf:reference/@query attribute
	private String query;

	public static ExternalReference newInstance(String sitename, String label, List<String> alternateLabel, String url, CodeLabel qualifier, String refidentifier, String query) {
		final ExternalReference e = new ExternalReference();
		e.setSitename(sitename);
		e.setLabel(label);
		e.setAlternateLabel(alternateLabel);
		e.setUrl(url);
		e.setQualifier(qualifier);
		e.setRefidentifier(refidentifier);
		e.setQuery(query);
		return e;
	}

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<String> getAlternateLabel() {
		return alternateLabel;
	}

	public void setAlternateLabel(List<String> alternateLabel) {
		this.alternateLabel = alternateLabel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public CodeLabel getQualifier() {
		return qualifier;
	}

	public void setQualifier(CodeLabel qualifier) {
		this.qualifier = qualifier;
	}

	public String getRefidentifier() {
		return refidentifier;
	}

	public void setRefidentifier(String refidentifier) {
		this.refidentifier = refidentifier;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

}
