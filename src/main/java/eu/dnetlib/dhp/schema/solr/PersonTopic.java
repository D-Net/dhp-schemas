package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.time.Year;

public class PersonTopic implements Serializable {

    private static final long serialVersionUID = 6866697695308782412L;

    private String value;

    private String schema;

    private String fromYear;

    private String toYear;

    public static PersonTopic newInstance(String value, String schema, String fromYear, String toYear) {
        final PersonTopic personTopic = new PersonTopic();
        personTopic.setValue(value);
        personTopic.setSchema(schema);
        personTopic.setFromYear(fromYear);
        personTopic.setToYear(toYear);
        return personTopic;
    }

    public PersonTopic() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }
}
