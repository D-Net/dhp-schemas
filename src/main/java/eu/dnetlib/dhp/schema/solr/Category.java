package eu.dnetlib.dhp.schema.solr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {

    private String id;
    private String label;
    private List<Concept> concept = new ArrayList<>();

    public static Category newInstance(String id, String label) {
        Category category = new Category();
        category.setId(id);
        category.setLabel(label);
        return category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Concept> getConcept() {
        return concept;
    }

    public void setConcept(List<Concept> concept) {
        this.concept = concept;
    }
}
