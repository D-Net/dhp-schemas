
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;

import eu.dnetlib.dhp.schema.common.ModelConstants;

public class Publication extends Result implements Serializable {

	private static final long serialVersionUID = 4155353046627214846L;

	// publication specific
	private Journal journal;

	public Publication() {
		setResulttype(ModelConstants.PUBLICATION_DEFAULT_RESULTTYPE);
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

}
