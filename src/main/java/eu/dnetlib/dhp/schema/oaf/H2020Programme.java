
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;

/**
 * To store information about the ec programme for the project. It has the following parameters:
 * - private String code to store the code of the programme
 * - private String description to store the description of the programme
 */
public class H2020Programme implements Serializable {

	private static final long serialVersionUID = -7851791644568592481L;
	private String code;
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
