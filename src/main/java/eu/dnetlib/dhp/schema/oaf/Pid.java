package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;

public class Pid implements Serializable {

    private static final long serialVersionUID = 8927505521598533507L;

    private String type;

    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
