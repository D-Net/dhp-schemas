
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;

public class OAIProvenance implements Serializable {

	private static final long serialVersionUID = -767252660700352729L;

	private OriginDescription originDescription;

	public OriginDescription getOriginDescription() {
		return originDescription;
	}

	public void setOriginDescription(OriginDescription originDescription) {
		this.originDescription = originDescription;
	}

}
