package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;

/**
 * Defines a mapping between the original resource types and the type code and label available in a given vocabulary.
 */
public class InstanceTypeMapping implements Serializable {

    private static final long serialVersionUID = -8209930054790495309L;
    /**
     * Original resource type, typically mapped from dc:type, datacite:resourceType.
     */
    private String originalType;

    /**
     * Contains the code of the resource type resulted from the mapping.
     */
    private String typeCode;

    /**
     * Contains the label of the resource type resulted from the mapping.
     */
    private String typeLabel;

    /**
     * Contains name of the vocabulary used to produce this resource type mapping.
     */
    private String vocabularyName;

    public String getOriginalType() {
        return originalType;
    }

    public void setOriginalType(String originalType) {
        this.originalType = originalType;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getVocabularyName() {
        return vocabularyName;
    }

    public void setVocabularyName(String vocabularyName) {
        this.vocabularyName = vocabularyName;
    }
}
