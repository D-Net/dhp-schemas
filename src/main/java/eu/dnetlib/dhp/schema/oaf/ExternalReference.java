
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

public class ExternalReference implements Serializable {

	private static final long serialVersionUID = -3711445600549155961L;
	// source
	private String sitename;

	// title
	private String label;

	// alternative labels
	private List<String> alternateLabel;

	// text()
	private String url;

	// type
	private Qualifier qualifier;

	// site internal identifier
	private String refidentifier;

	// maps the oaf:reference/@query attribute
	private String query;

	// ExternalReferences might be also inferred
	private DataInfo dataInfo;

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<String> getAlternateLabel() {
		return alternateLabel;
	}

	public void setAlternateLabel(List<String> alternateLabel) {
		this.alternateLabel = alternateLabel;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Qualifier getQualifier() {
		return qualifier;
	}

	public void setQualifier(Qualifier qualifier) {
		this.qualifier = qualifier;
	}

	public String getRefidentifier() {
		return refidentifier;
	}

	public void setRefidentifier(String refidentifier) {
		this.refidentifier = refidentifier;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public DataInfo getDataInfo() {
		return dataInfo;
	}

	public void setDataInfo(DataInfo dataInfo) {
		this.dataInfo = dataInfo;
	}

}
