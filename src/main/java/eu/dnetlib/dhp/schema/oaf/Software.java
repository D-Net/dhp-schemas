
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

import eu.dnetlib.dhp.schema.common.ModelConstants;

public class Software extends Result implements Serializable {

	private static final long serialVersionUID = -3096641763359312576L;

	private List<Field<String>> documentationUrl;

	// candidate for removal
	private List<StructuredProperty> license;

	// candidate for removal
	private Field<String> codeRepositoryUrl;

	private Qualifier programmingLanguage;

	public Software() {
		setResulttype(ModelConstants.SOFTWARE_DEFAULT_RESULTTYPE);
	}

	public List<Field<String>> getDocumentationUrl() {
		return documentationUrl;
	}

	public void setDocumentationUrl(List<Field<String>> documentationUrl) {
		this.documentationUrl = documentationUrl;
	}

	public List<StructuredProperty> getLicense() {
		return license;
	}

	public void setLicense(List<StructuredProperty> license) {
		this.license = license;
	}

	public Field<String> getCodeRepositoryUrl() {
		return codeRepositoryUrl;
	}

	public void setCodeRepositoryUrl(Field<String> codeRepositoryUrl) {
		this.codeRepositoryUrl = codeRepositoryUrl;
	}

	public Qualifier getProgrammingLanguage() {
		return programmingLanguage;
	}

	public void setProgrammingLanguage(Qualifier programmingLanguage) {
		this.programmingLanguage = programmingLanguage;
	}

}
