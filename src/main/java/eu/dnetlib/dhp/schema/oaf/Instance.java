
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

public class Instance implements Serializable {

	private static final long serialVersionUID = 3918544870298859300L;
	private Field<String> license;

	private AccessRight accessright;

	private Qualifier instancetype;

	private List<InstanceTypeMapping> instanceTypeMapping;

	private KeyValue hostedby;

	private List<String> url;

	// other research products specifc
	private String distributionlocation;

	private KeyValue collectedfrom;

	private List<StructuredProperty> pid;

	private List<StructuredProperty> alternateIdentifier;

	private Field<String> dateofacceptance;

	// ( article | book ) processing charges. Defined here to cope with possible wrongly typed
	// results
	private Field<String> processingchargeamount;

	// currency - alphabetic code describe in ISO-4217. Defined here to cope with possible wrongly
	// typed results
	private Field<String> processingchargecurrency;

	private Qualifier refereed; // peer-review status

	private List<Measure> measures;

	/**
	 * Direct fulltext URL.
	 */
	private String fulltext;

	public Field<String> getLicense() {
		return license;
	}

	public void setLicense(Field<String> license) {
		this.license = license;
	}

	public AccessRight getAccessright() {
		return accessright;
	}

	public void setAccessright(AccessRight accessright) {
		this.accessright = accessright;
	}

	public Qualifier getInstancetype() {
		return instancetype;
	}

	public void setInstancetype(Qualifier instancetype) {
		this.instancetype = instancetype;
	}

	public List<InstanceTypeMapping> getInstanceTypeMapping() {
		return instanceTypeMapping;
	}

	public void setInstanceTypeMapping(List<InstanceTypeMapping> instanceTypeMapping) {
		this.instanceTypeMapping = instanceTypeMapping;
	}

	public KeyValue getHostedby() {
		return hostedby;
	}

	public void setHostedby(KeyValue hostedby) {
		this.hostedby = hostedby;
	}

	public List<String> getUrl() {
		return url;
	}

	public void setUrl(List<String> url) {
		this.url = url;
	}

	public String getDistributionlocation() {
		return distributionlocation;
	}

	public void setDistributionlocation(String distributionlocation) {
		this.distributionlocation = distributionlocation;
	}

	public KeyValue getCollectedfrom() {
		return collectedfrom;
	}

	public void setCollectedfrom(KeyValue collectedfrom) {
		this.collectedfrom = collectedfrom;
	}

	public List<StructuredProperty> getPid() {
		return pid;
	}

	public void setPid(List<StructuredProperty> pid) {
		this.pid = pid;
	}

	public Field<String> getDateofacceptance() {
		return dateofacceptance;
	}

	public void setDateofacceptance(Field<String> dateofacceptance) {
		this.dateofacceptance = dateofacceptance;
	}

	public List<StructuredProperty> getAlternateIdentifier() {
		return alternateIdentifier;
	}

	public void setAlternateIdentifier(List<StructuredProperty> alternateIdentifier) {
		this.alternateIdentifier = alternateIdentifier;
	}

	public Field<String> getProcessingchargeamount() {
		return processingchargeamount;
	}

	public void setProcessingchargeamount(Field<String> processingchargeamount) {
		this.processingchargeamount = processingchargeamount;
	}

	public Field<String> getProcessingchargecurrency() {
		return processingchargecurrency;
	}

	public void setProcessingchargecurrency(Field<String> processingchargecurrency) {
		this.processingchargecurrency = processingchargecurrency;
	}

	public Qualifier getRefereed() {
		return refereed;
	}

	public void setRefereed(Qualifier refereed) {
		this.refereed = refereed;
	}

	public List<Measure> getMeasures() {
		return measures;
	}

	public void setMeasures(List<Measure> measures) {
		this.measures = measures;
	}

	public String getFulltext() {
		return fulltext;
	}

	public void setFulltext(String fulltext) {
		this.fulltext = fulltext;
	}

}
