
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

/**
 * Represent a measure, must be further described by a system available resource providing name and descriptions.
 */
public class Measure implements Serializable {

	private static final long serialVersionUID = 8383308453679722661L;
	/**
	 * Unique measure identifier.
	 */
	private String id;

	/**
	 * List of units associated with this measure. KeyValue provides a pair to store the label (key) and the value, plus
	 * common provenance information.
	 */
	private List<KeyValue> unit;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<KeyValue> getUnit() {
		return unit;
	}

	public void setUnit(List<KeyValue> unit) {
		this.unit = unit;
	}

}
