
package eu.dnetlib.dhp.schema.oaf;

public class Country extends Qualifier {

	private static final long serialVersionUID = 1587334433446567175L;
	private DataInfo dataInfo;

	public DataInfo getDataInfo() {
		return dataInfo;
	}

	public void setDataInfo(DataInfo dataInfo) {
		this.dataInfo = dataInfo;
	}

}
