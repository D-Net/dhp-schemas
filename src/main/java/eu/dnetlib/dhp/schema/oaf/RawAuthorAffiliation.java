package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

public class RawAuthorAffiliation implements Serializable {

    private static final long serialVersionUID = -4248069698947092558L;

    private String rawAffiliationString;

    private List<Pid> pids;

    public String getRawAffiliationString() {
        return rawAffiliationString;
    }

    public void setRawAffiliationString(String rawAffiliationString) {
        this.rawAffiliationString = rawAffiliationString;
    }

    public List<Pid> getPids() {
        return pids;
    }

    public void setPids(List<Pid> pids) {
        this.pids = pids;
    }
}

