
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The type Result.
 */
public class Result extends OafEntity implements Serializable {

	private static final long serialVersionUID = 5597290552671699408L;

	/**
	 * ( article | book ) processing charges.
 	 */
	private Field<String> processingchargeamount;

	/**
	 * currency - alphabetic code describe in ISO-4217.
	 */
	private Field<String> processingchargecurrency;

	/**
	 * The Author.
	 */
	private List<Author> author;

	/**
	 * The Resulttype.
	 */
// resulttype allows subclassing results into publications | datasets | software
	private Qualifier resulttype;

	/**
	 * Temporary field suporting the analysis of the new COAR-based resource types
	 */
	private Qualifier metaResourceType;

	/**
	 * The Language.
	 */
// common fields
	private Qualifier language;

	/**
	 * The Country.
	 */
	private List<Country> country;

	/**
	 * The Subject.
	 */
	private List<Subject> subject;

	/**
	 * The Title.
	 */
	private List<StructuredProperty> title;

	/**
	 * The Relevantdate.
	 */
	private List<StructuredProperty> relevantdate;

	/**
	 * The Description.
	 */
	private List<Field<String>> description;

	/**
	 * The Dateofacceptance.
	 */
	private Field<String> dateofacceptance;

	/**
	 * The Publisher.
	 */
	private Field<String> publisher;

	/**
	 * The Embargoenddate.
	 */
	private Field<String> embargoenddate;

	/**
	 * The Source.
	 */
	private List<Field<String>> source;

	/**
	 * The list of direct fulltext URLs.
	 */
	private List<Field<String>> fulltext;

	/**
	 * The Format.
	 */
	private List<Field<String>> format;

	/**
	 * The Contributor.
	 */
	private List<Field<String>> contributor;

	/**
	 * The Resourcetype.
	 */
	private Qualifier resourcetype;

	/**
	 * The Coverage.
	 */
	private List<Field<String>> coverage;

	/**
	 * The Bestaccessright.
	 */
	private Qualifier bestaccessright;

	/**
	 * The External reference.
	 */
	private List<ExternalReference> externalReference;

	/**
	 * The most representative instanceType among the instances.
	 */
	private Qualifier bestInstancetype;

	/**
	 * The Instance.
	 */
	private List<Instance> instance;

	/**
	 * EOSC Interoperability Framework Guidelines
	 */
	private List<EoscIfGuidelines> eoscifguidelines;

	@JsonProperty("isGreen")
	private Boolean isGreen;

	private OpenAccessColor openAccessColor;

	@JsonProperty("isInDiamondJournal")
	private Boolean isInDiamondJournal;

	private Boolean publiclyFunded;

	private String transformativeAgreement;

	public String getTransformativeAgreement() {
		return transformativeAgreement;
	}

	public void setTransformativeAgreement(String transformativeAgreement) {
		this.transformativeAgreement = transformativeAgreement;
	}

	public Field<String> getProcessingchargeamount() {
		return processingchargeamount;
	}

	public void setProcessingchargeamount(Field<String> processingchargeamount) {
		this.processingchargeamount = processingchargeamount;
	}

	public Field<String> getProcessingchargecurrency() {
		return processingchargecurrency;
	}

	public void setProcessingchargecurrency(Field<String> processingchargecurrency) {
		this.processingchargecurrency = processingchargecurrency;
	}

	/**
	 * Gets author.
	 *
	 * @return the author
	 */
	public List<Author> getAuthor() {
		return author;
	}

	/**
	 * Sets author.
	 *
	 * @param author the author
	 */
	public void setAuthor(List<Author> author) {
		this.author = author;
	}

	/**
	 * Gets resulttype.
	 *
	 * @return the resulttype
	 */
	public Qualifier getResulttype() {
		return resulttype;
	}

	/**
	 * Sets resulttype.
	 *
	 * @param resulttype the resulttype
	 */
	public void setResulttype(Qualifier resulttype) {
		this.resulttype = resulttype;
	}

	public Qualifier getMetaResourceType() {
		return metaResourceType;
	}

	public void setMetaResourceType(Qualifier metaResourceType) {
		this.metaResourceType = metaResourceType;
	}

	/**
	 * Gets language.
	 *
	 * @return the language
	 */
	public Qualifier getLanguage() {
		return language;
	}

	/**
	 * Sets language.
	 *
	 * @param language the language
	 */
	public void setLanguage(Qualifier language) {
		this.language = language;
	}

	/**
	 * Gets country.
	 *
	 * @return the country
	 */
	public List<Country> getCountry() {
		return country;
	}

	/**
	 * Sets country.
	 *
	 * @param country the country
	 */
	public void setCountry(List<Country> country) {
		this.country = country;
	}

	/**
	 * Gets subject.
	 *
	 * @return the subject
	 */
	public List<Subject> getSubject() {
		return subject;
	}

	/**
	 * Sets subject.
	 *
	 * @param subject the subject
	 */
	public void setSubject(List<Subject> subject) {
		this.subject = subject;
	}

	/**
	 * Gets title.
	 *
	 * @return the title
	 */
	public List<StructuredProperty> getTitle() {
		return title;
	}

	/**
	 * Sets title.
	 *
	 * @param title the title
	 */
	public void setTitle(List<StructuredProperty> title) {
		this.title = title;
	}

	/**
	 * Gets relevantdate.
	 *
	 * @return the relevantdate
	 */
	public List<StructuredProperty> getRelevantdate() {
		return relevantdate;
	}

	/**
	 * Sets relevantdate.
	 *
	 * @param relevantdate the relevantdate
	 */
	public void setRelevantdate(List<StructuredProperty> relevantdate) {
		this.relevantdate = relevantdate;
	}

	/**
	 * Gets description.
	 *
	 * @return the description
	 */
	public List<Field<String>> getDescription() {
		return description;
	}

	/**
	 * Sets description.
	 *
	 * @param description the description
	 */
	public void setDescription(List<Field<String>> description) {
		this.description = description;
	}

	/**
	 * Gets dateofacceptance.
	 *
	 * @return the dateofacceptance
	 */
	public Field<String> getDateofacceptance() {
		return dateofacceptance;
	}

	/**
	 * Sets dateofacceptance.
	 *
	 * @param dateofacceptance the dateofacceptance
	 */
	public void setDateofacceptance(Field<String> dateofacceptance) {
		this.dateofacceptance = dateofacceptance;
	}

	/**
	 * Gets publisher.
	 *
	 * @return the publisher
	 */
	public Field<String> getPublisher() {
		return publisher;
	}

	/**
	 * Sets publisher.
	 *
	 * @param publisher the publisher
	 */
	public void setPublisher(Field<String> publisher) {
		this.publisher = publisher;
	}

	/**
	 * Gets embargoenddate.
	 *
	 * @return the embargoenddate
	 */
	public Field<String> getEmbargoenddate() {
		return embargoenddate;
	}

	/**
	 * Sets embargoenddate.
	 *
	 * @param embargoenddate the embargoenddate
	 */
	public void setEmbargoenddate(Field<String> embargoenddate) {
		this.embargoenddate = embargoenddate;
	}

	/**
	 * Gets source.
	 *
	 * @return the source
	 */
	public List<Field<String>> getSource() {
		return source;
	}

	/**
	 * Sets source.
	 *
	 * @param source the source
	 */
	public void setSource(List<Field<String>> source) {
		this.source = source;
	}

	/**
	 * Gets fulltext.
	 *
	 * @return the fulltext
	 */
	public List<Field<String>> getFulltext() {
		return fulltext;
	}

	/**
	 * Sets fulltext.
	 *
	 * @param fulltext the fulltext
	 */
	public void setFulltext(List<Field<String>> fulltext) {
		this.fulltext = fulltext;
	}

	/**
	 * Gets format.
	 *
	 * @return the format
	 */
	public List<Field<String>> getFormat() {
		return format;
	}

	/**
	 * Sets format.
	 *
	 * @param format the format
	 */
	public void setFormat(List<Field<String>> format) {
		this.format = format;
	}

	/**
	 * Gets contributor.
	 *
	 * @return the contributor
	 */
	public List<Field<String>> getContributor() {
		return contributor;
	}

	/**
	 * Sets contributor.
	 *
	 * @param contributor the contributor
	 */
	public void setContributor(List<Field<String>> contributor) {
		this.contributor = contributor;
	}

	/**
	 * Gets resourcetype.
	 *
	 * @return the resourcetype
	 */
	public Qualifier getResourcetype() {
		return resourcetype;
	}

	/**
	 * Sets resourcetype.
	 *
	 * @param resourcetype the resourcetype
	 */
	public void setResourcetype(Qualifier resourcetype) {
		this.resourcetype = resourcetype;
	}

	/**
	 * Gets coverage.
	 *
	 * @return the coverage
	 */
	public List<Field<String>> getCoverage() {
		return coverage;
	}

	/**
	 * Sets coverage.
	 *
	 * @param coverage the coverage
	 */
	public void setCoverage(List<Field<String>> coverage) {
		this.coverage = coverage;
	}

	/**
	 * Gets bestaccessright.
	 *
	 * @return the bestaccessright
	 */
	public Qualifier getBestaccessright() {
		return bestaccessright;
	}

	/**
	 * Sets bestaccessright.
	 *
	 * @param bestaccessright the bestaccessright
	 */
	public void setBestaccessright(Qualifier bestaccessright) {
		this.bestaccessright = bestaccessright;
	}

	/**
	 * Gets external reference.
	 *
	 * @return the external reference
	 */
	public List<ExternalReference> getExternalReference() {
		return externalReference;
	}

	/**
	 * Sets external reference.
	 *
	 * @param externalReference the external reference
	 */
	public void setExternalReference(List<ExternalReference> externalReference) {
		this.externalReference = externalReference;
	}

	public Qualifier getBestInstancetype() {
		return bestInstancetype;
	}

	public void setBestInstancetype(Qualifier bestInstancetype) {
		this.bestInstancetype = bestInstancetype;
	}

	/**
	 * Gets instance.
	 *
	 * @return the instance
	 */
	public List<Instance> getInstance() {
		return instance;
	}

	/**
	 * Sets instance.
	 *
	 * @param instance the instance
	 */
	public void setInstance(List<Instance> instance) {
		this.instance = instance;
	}

	public List<EoscIfGuidelines> getEoscifguidelines() {
		return eoscifguidelines;
	}

	public void setEoscifguidelines(List<EoscIfGuidelines> eoscifguidelines) {
		this.eoscifguidelines = eoscifguidelines;
	}

	public Boolean getIsGreen() {
		return isGreen;
	}

	public void setIsGreen(Boolean green) {
		isGreen = green;
	}

	public OpenAccessColor getOpenAccessColor() {
		return openAccessColor;
	}

	public void setOpenAccessColor(OpenAccessColor openAccessColor) {
		this.openAccessColor = openAccessColor;
	}

	public Boolean getIsInDiamondJournal() {
		return isInDiamondJournal;
	}

	public void setIsInDiamondJournal(Boolean inDiamondJournal) {
		isInDiamondJournal = inDiamondJournal;
	}

	public Boolean getPubliclyFunded() {
		return publiclyFunded;
	}

	public void setPubliclyFunded(Boolean publiclyFunded) {
		this.publiclyFunded = publiclyFunded;
	}

}
