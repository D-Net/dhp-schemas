
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.*;

public abstract class OafEntity extends Oaf implements Serializable {

	private static final long serialVersionUID = -7793594664115821897L;

	private String id;

	/**
	 * MergedIds contains the list of the OpenAIRE IDs of the records merged into this one.
	 */
	private List<String> mergedIds;

	private List<String> originalId;

	private List<StructuredProperty> pid;

	private String dateofcollection;

	private String dateoftransformation;

	private List<ExtraInfo> extraInfo;

	private OAIProvenance oaiprovenance;

	/**
	 * The Measures.
	 */
	private List<Measure> measures;

	/**
	 * The Context.
	 */
	private List<Context> context;

	/**
	 * Gets context.
	 *
	 * @return the context
	 */
	public List<Context> getContext() {
		return context;
	}

	/**
	 * Sets context.
	 *
	 * @param context the context
	 */
	public void setContext(List<Context> context) {
		this.context = context;
	}

	/**
	 * Gets measures.
	 *
	 * @return the measures
	 */
	public List<Measure> getMeasures() {
		return measures;
	}

	/**
	 * Sets measures.
	 *
	 * @param measures the measures
	 */
	public void setMeasures(List<Measure> measures) {
		this.measures = measures;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getOriginalId() {
		return originalId;
	}

	public List<String> getMergedIds() {
		return mergedIds;
	}

	public void setMergedIds(List<String> mergedIds) {
		this.mergedIds = mergedIds;
	}

	public void setOriginalId(List<String> originalId) {
		this.originalId = originalId;
	}

	public List<StructuredProperty> getPid() {
		return pid;
	}

	public void setPid(List<StructuredProperty> pid) {
		this.pid = pid;
	}

	public String getDateofcollection() {
		return dateofcollection;
	}

	public void setDateofcollection(String dateofcollection) {
		this.dateofcollection = dateofcollection;
	}

	public String getDateoftransformation() {
		return dateoftransformation;
	}

	public void setDateoftransformation(String dateoftransformation) {
		this.dateoftransformation = dateoftransformation;
	}

	public List<ExtraInfo> getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(List<ExtraInfo> extraInfo) {
		this.extraInfo = extraInfo;
	}

	public OAIProvenance getOaiprovenance() {
		return oaiprovenance;
	}

	public void setOaiprovenance(OAIProvenance oaiprovenance) {
		this.oaiprovenance = oaiprovenance;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof OafEntity))
			return false;
		OafEntity oafEntity = (OafEntity) o;
		return Objects.equals(getId(), oafEntity.getId());
	}

}
