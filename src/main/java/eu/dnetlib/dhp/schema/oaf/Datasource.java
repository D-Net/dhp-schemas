
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

public class Datasource extends OafEntity implements Serializable {

	private static final long serialVersionUID = 1019089598408414496L;

	@Deprecated
	private Qualifier datasourcetype;

	@Deprecated
	private Qualifier datasourcetypeui;

	private Qualifier eosctype; // Data Source | Service

	private Qualifier eoscdatasourcetype;

	private Qualifier openairecompatibility;

	private Field<String> officialname;

	private Field<String> englishname;

	private Field<String> websiteurl;

	private Field<String> logourl;

	private Field<String> contactemail;

	private Field<String> namespaceprefix;

	private Field<String> latitude;

	private Field<String> longitude;

	private Field<String> dateofvalidation;

	private Field<String> description;

	private List<StructuredProperty> subjects;

	// opendoar specific fields (od*)
	@Deprecated
	private Field<String> odnumberofitems;

	@Deprecated
	private Field<String> odnumberofitemsdate;

	@Deprecated
	private Field<String> odpolicies;

	@Deprecated
	private List<Field<String>> odlanguages;

	private List<String> languages;

	@Deprecated
	private List<Field<String>> odcontenttypes;

	private List<Field<String>> accessinfopackage;

	// re3data fields
	private Field<String> releasestartdate;

	private Field<String> releaseenddate;

	private Field<String> missionstatementurl;

	@Deprecated
	private Field<Boolean> dataprovider;

	@Deprecated
	private Field<Boolean> serviceprovider;

	// {open, restricted or closed}
	private Field<String> databaseaccesstype;

	// {open, restricted or closed}
	private Field<String> datauploadtype;

	// {feeRequired, registration, other}
	private Field<String> databaseaccessrestriction;

	// {feeRequired, registration, other}
	private Field<String> datauploadrestriction;

	@Deprecated
	private Field<Boolean> versioning;

	private Boolean versioncontrol;

	private Field<String> citationguidelineurl;

	private Field<String> pidsystems;

	private Field<String> certificates;

	@Deprecated
	private List<KeyValue> policies;

	private Journal journal;

	// New field for EOSC
	private List<String> researchentitytypes;

	// New field for EOSC
	private List<String> providedproducttypes;

	// New field for EOSC
	private Qualifier jurisdiction;

	// New field for EOSC
	private Boolean thematic;

	// New field for EOSC
	private List<Qualifier> contentpolicies;

	private String submissionpolicyurl;

	private String preservationpolicyurl;

	private List<String> researchproductaccesspolicies;

	private List<String> researchproductmetadataaccesspolicies;

	private Boolean consenttermsofuse;

	private Boolean fulltextdownload;

	private String consenttermsofusedate;

	private String lastconsenttermsofusedate;

	/**
	 * EOSC Interoperability Framework Guidelines
	 */
	private List<EoscIfGuidelines> eoscifguidelines;

	public Qualifier getDatasourcetype() {
		return datasourcetype;
	}

	public void setDatasourcetype(final Qualifier datasourcetype) {
		this.datasourcetype = datasourcetype;
	}

	public Qualifier getDatasourcetypeui() {
		return datasourcetypeui;
	}

	public void setDatasourcetypeui(final Qualifier datasourcetypeui) {
		this.datasourcetypeui = datasourcetypeui;
	}

	public Qualifier getEosctype() {
		return eosctype;
	}

	public void setEosctype(Qualifier eosctype) {
		this.eosctype = eosctype;
	}

	public Qualifier getEoscdatasourcetype() {
		return eoscdatasourcetype;
	}

	public void setEoscdatasourcetype(Qualifier eoscdatasourcetype) {
		this.eoscdatasourcetype = eoscdatasourcetype;
	}

	public Qualifier getOpenairecompatibility() {
		return openairecompatibility;
	}

	public void setOpenairecompatibility(final Qualifier openairecompatibility) {
		this.openairecompatibility = openairecompatibility;
	}

	public Field<String> getOfficialname() {
		return officialname;
	}

	public void setOfficialname(final Field<String> officialname) {
		this.officialname = officialname;
	}

	public Field<String> getEnglishname() {
		return englishname;
	}

	public void setEnglishname(final Field<String> englishname) {
		this.englishname = englishname;
	}

	public Field<String> getWebsiteurl() {
		return websiteurl;
	}

	public void setWebsiteurl(final Field<String> websiteurl) {
		this.websiteurl = websiteurl;
	}

	public Field<String> getLogourl() {
		return logourl;
	}

	public void setLogourl(final Field<String> logourl) {
		this.logourl = logourl;
	}

	public Field<String> getContactemail() {
		return contactemail;
	}

	public void setContactemail(final Field<String> contactemail) {
		this.contactemail = contactemail;
	}

	public Field<String> getNamespaceprefix() {
		return namespaceprefix;
	}

	public void setNamespaceprefix(final Field<String> namespaceprefix) {
		this.namespaceprefix = namespaceprefix;
	}

	public Field<String> getLatitude() {
		return latitude;
	}

	public void setLatitude(final Field<String> latitude) {
		this.latitude = latitude;
	}

	public Field<String> getLongitude() {
		return longitude;
	}

	public void setLongitude(final Field<String> longitude) {
		this.longitude = longitude;
	}

	public Field<String> getDateofvalidation() {
		return dateofvalidation;
	}

	public void setDateofvalidation(final Field<String> dateofvalidation) {
		this.dateofvalidation = dateofvalidation;
	}

	public Field<String> getDescription() {
		return description;
	}

	public void setDescription(final Field<String> description) {
		this.description = description;
	}

	public List<StructuredProperty> getSubjects() {
		return subjects;
	}

	public void setSubjects(final List<StructuredProperty> subjects) {
		this.subjects = subjects;
	}

	public Field<String> getOdnumberofitems() {
		return odnumberofitems;
	}

	public void setOdnumberofitems(final Field<String> odnumberofitems) {
		this.odnumberofitems = odnumberofitems;
	}

	public Field<String> getOdnumberofitemsdate() {
		return odnumberofitemsdate;
	}

	public void setOdnumberofitemsdate(final Field<String> odnumberofitemsdate) {
		this.odnumberofitemsdate = odnumberofitemsdate;
	}

	public Field<String> getOdpolicies() {
		return odpolicies;
	}

	public void setOdpolicies(final Field<String> odpolicies) {
		this.odpolicies = odpolicies;
	}

	public List<Field<String>> getOdlanguages() {
		return odlanguages;
	}

	public void setOdlanguages(final List<Field<String>> odlanguages) {
		this.odlanguages = odlanguages;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(final List<String> languages) {
		this.languages = languages;
	}

	public List<Field<String>> getOdcontenttypes() {
		return odcontenttypes;
	}

	public void setOdcontenttypes(final List<Field<String>> odcontenttypes) {
		this.odcontenttypes = odcontenttypes;
	}

	public List<Field<String>> getAccessinfopackage() {
		return accessinfopackage;
	}

	public void setAccessinfopackage(final List<Field<String>> accessinfopackage) {
		this.accessinfopackage = accessinfopackage;
	}

	public Field<String> getReleasestartdate() {
		return releasestartdate;
	}

	public void setReleasestartdate(final Field<String> releasestartdate) {
		this.releasestartdate = releasestartdate;
	}

	public Field<String> getReleaseenddate() {
		return releaseenddate;
	}

	public void setReleaseenddate(final Field<String> releaseenddate) {
		this.releaseenddate = releaseenddate;
	}

	public Field<String> getMissionstatementurl() {
		return missionstatementurl;
	}

	public void setMissionstatementurl(final Field<String> missionstatementurl) {
		this.missionstatementurl = missionstatementurl;
	}

	public Field<Boolean> getDataprovider() {
		return dataprovider;
	}

	public void setDataprovider(final Field<Boolean> dataprovider) {
		this.dataprovider = dataprovider;
	}

	public Field<Boolean> getServiceprovider() {
		return serviceprovider;
	}

	public void setServiceprovider(final Field<Boolean> serviceprovider) {
		this.serviceprovider = serviceprovider;
	}

	public Field<String> getDatabaseaccesstype() {
		return databaseaccesstype;
	}

	public void setDatabaseaccesstype(final Field<String> databaseaccesstype) {
		this.databaseaccesstype = databaseaccesstype;
	}

	public Field<String> getDatauploadtype() {
		return datauploadtype;
	}

	public void setDatauploadtype(final Field<String> datauploadtype) {
		this.datauploadtype = datauploadtype;
	}

	public Field<String> getDatabaseaccessrestriction() {
		return databaseaccessrestriction;
	}

	public void setDatabaseaccessrestriction(final Field<String> databaseaccessrestriction) {
		this.databaseaccessrestriction = databaseaccessrestriction;
	}

	public Field<String> getDatauploadrestriction() {
		return datauploadrestriction;
	}

	public void setDatauploadrestriction(final Field<String> datauploadrestriction) {
		this.datauploadrestriction = datauploadrestriction;
	}

	public Field<Boolean> getVersioning() {
		return versioning;
	}

	public void setVersioning(final Field<Boolean> versioning) {
		this.versioning = versioning;
	}

	public Boolean getVersioncontrol() {
		return versioncontrol;
	}

	public void setVersioncontrol(Boolean versioncontrol) {
		this.versioncontrol = versioncontrol;
	}

	public Field<String> getCitationguidelineurl() {
		return citationguidelineurl;
	}

	public void setCitationguidelineurl(final Field<String> citationguidelineurl) {
		this.citationguidelineurl = citationguidelineurl;
	}

	public Field<String> getPidsystems() {
		return pidsystems;
	}

	public void setPidsystems(final Field<String> pidsystems) {
		this.pidsystems = pidsystems;
	}

	public Field<String> getCertificates() {
		return certificates;
	}

	public void setCertificates(final Field<String> certificates) {
		this.certificates = certificates;
	}

	public List<KeyValue> getPolicies() {
		return policies;
	}

	public void setPolicies(final List<KeyValue> policies) {
		this.policies = policies;
	}

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(final Journal journal) {
		this.journal = journal;
	}

	public List<String> getResearchentitytypes() {
		return researchentitytypes;
	}

	public void setResearchentitytypes(final List<String> researchentitytypes) {
		this.researchentitytypes = researchentitytypes;
	}

	public List<String> getProvidedproducttypes() {
		return providedproducttypes;
	}

	public void setProvidedproducttypes(final List<String> providedproducttypes) {
		this.providedproducttypes = providedproducttypes;
	}

	public Qualifier getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(final Qualifier jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public Boolean getThematic() {
		return thematic;
	}

	public void setThematic(final Boolean thematic) {
		this.thematic = thematic;
	}

	public List<Qualifier> getContentpolicies() {
		return contentpolicies;
	}

	public void setContentpolicies(final List<Qualifier> contentpolicies) {
		this.contentpolicies = contentpolicies;
	}

	public String getSubmissionpolicyurl() {
		return submissionpolicyurl;
	}

	public void setSubmissionpolicyurl(String submissionpolicyurl) {
		this.submissionpolicyurl = submissionpolicyurl;
	}

	public String getPreservationpolicyurl() {
		return preservationpolicyurl;
	}

	public void setPreservationpolicyurl(String preservationpolicyurl) {
		this.preservationpolicyurl = preservationpolicyurl;
	}

	public List<String> getResearchproductaccesspolicies() {
		return researchproductaccesspolicies;
	}

	public void setResearchproductaccesspolicies(List<String> researchproductaccesspolicies) {
		this.researchproductaccesspolicies = researchproductaccesspolicies;
	}

	public List<String> getResearchproductmetadataaccesspolicies() {
		return researchproductmetadataaccesspolicies;
	}

	public void setResearchproductmetadataaccesspolicies(List<String> researchproductmetadataaccesspolicies) {
		this.researchproductmetadataaccesspolicies = researchproductmetadataaccesspolicies;
	}

	public Boolean getConsenttermsofuse() {
		return consenttermsofuse;
	}

	public void setConsenttermsofuse(final Boolean consenttermsofuse) {
		this.consenttermsofuse = consenttermsofuse;
	}

	public String getLastconsenttermsofusedate() {
		return lastconsenttermsofusedate;
	}

	public void setLastconsenttermsofusedate(String lastconsenttermsofusedate) {
		this.lastconsenttermsofusedate = lastconsenttermsofusedate;
	}

	public Boolean getFulltextdownload() {
		return fulltextdownload;
	}

	public void setFulltextdownload(final Boolean fulltextdownload) {
		this.fulltextdownload = fulltextdownload;
	}

	public String getConsenttermsofusedate() {
		return consenttermsofusedate;
	}

	public void setConsenttermsofusedate(final String consenttermsofusedate) {
		this.consenttermsofusedate = consenttermsofusedate;
	}

	public List<EoscIfGuidelines> getEoscifguidelines() {
		return eoscifguidelines;
	}

	public void setEoscifguidelines(List<EoscIfGuidelines> eoscifguidelines) {
		this.eoscifguidelines = eoscifguidelines;
	}

}
