package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

public class Person extends OafEntity implements Serializable {

    private static final long serialVersionUID = -2366333710489222265L;

    private String givenName;

    private String familyName;

    private List<String> alternativeNames;

    private String biography;

    private List<PersonTopic> subject;

    private Boolean consent;

    public Person() {
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public List<String> getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(List<String> alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public List<PersonTopic> getSubject() {
        return subject;
    }

    public void setSubject(List<PersonTopic> subject) {
        this.subject = subject;
    }

    public Boolean getConsent() {
        return consent;
    }

    public void setConsent(Boolean consent) {
        this.consent = consent;
    }
}
