package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;

public class PersonTopic implements Serializable {

    private static final long serialVersionUID = 102011326860637199L;

    private String value;

    private String schema;

    private String fromYear;

    private String toYear;

    public PersonTopic() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }
}
