
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.*;

public class Author implements Serializable {

	private static final long serialVersionUID = 1625492739044457437L;

	private String fullname;

	private String name;

	private String surname;

	// START WITH 1
	private Integer rank;

	private List<StructuredProperty> pid;

	private List<String> rawAffiliationString;

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public List<StructuredProperty> getPid() {
		return pid;
	}

	public void setPid(List<StructuredProperty> pid) {
		this.pid = pid;
	}

	public List<String> getRawAffiliationString() {
		return rawAffiliationString;
	}

	public void setRawAffiliationString(List<String> rawAffiliationString) {
		this.rawAffiliationString = rawAffiliationString;
	}
}
