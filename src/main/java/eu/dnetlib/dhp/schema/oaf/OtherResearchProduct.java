
package eu.dnetlib.dhp.schema.oaf;

import java.io.Serializable;
import java.util.List;

import eu.dnetlib.dhp.schema.common.ModelConstants;

public class OtherResearchProduct extends Result implements Serializable {

	private static final long serialVersionUID = -7611546868867175491L;

	private List<Field<String>> contactperson;

	private List<Field<String>> contactgroup;

	private List<Field<String>> tool;

	public OtherResearchProduct() {
		setResulttype(ModelConstants.ORP_DEFAULT_RESULTTYPE);
	}

	public List<Field<String>> getContactperson() {
		return contactperson;
	}

	public void setContactperson(List<Field<String>> contactperson) {
		this.contactperson = contactperson;
	}

	public List<Field<String>> getContactgroup() {
		return contactgroup;
	}

	public void setContactgroup(List<Field<String>> contactgroup) {
		this.contactgroup = contactgroup;
	}

	public List<Field<String>> getTool() {
		return tool;
	}

	public void setTool(List<Field<String>> tool) {
		this.tool = tool;
	}

}
