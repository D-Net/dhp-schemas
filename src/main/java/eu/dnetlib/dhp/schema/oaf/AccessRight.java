
package eu.dnetlib.dhp.schema.oaf;

/**
 * This class models the access rights of research products.
 */
public class AccessRight extends Qualifier {

	private static final long serialVersionUID = -8945177777173510134L;

	private OpenAccessRoute openAccessRoute;

	public OpenAccessRoute getOpenAccessRoute() {
		return openAccessRoute;
	}

	public void setOpenAccessRoute(OpenAccessRoute openAccessRoute) {
		this.openAccessRoute = openAccessRoute;
	}

}
