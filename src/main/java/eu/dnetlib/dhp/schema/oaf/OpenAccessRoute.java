
package eu.dnetlib.dhp.schema.oaf;

/**
 * This Enum models the OpenAccess status, currently including only the values from Unpaywall
 *
 * https://support.unpaywall.org/support/solutions/articles/44001777288-what-do-the-types-of-oa-status-green-gold-hybrid-and-bronze-mean-
 */
public enum OpenAccessRoute {

	gold, green, hybrid, bronze

}
