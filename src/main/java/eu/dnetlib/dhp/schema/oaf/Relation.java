
package eu.dnetlib.dhp.schema.oaf;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Relation models any edge between two nodes in the OpenAIRE graph. It has a source id and a target id pointing to
 * graph node identifiers and it is further characterised by the semantic of the link through the fields relType,
 * subRelType and relClass. Provenance information is modeled according to the dataInfo element and collectedFrom, while
 * individual relationship types can provide extra information via the properties field.
 */
public class Relation extends Oaf {

	private static final long serialVersionUID = -7061565324218172207L;

	/**
	 * Main relationship classifier, values include 'resultResult', 'resultProject', 'resultOrganization', etc.
	 */
	private String relType;

	/**
	 * Further classifies a relationship, values include 'affiliation', 'similarity', 'supplement', etc.
	 */
	private String subRelType;

	/**
	 * Indicates the direction of the relationship, values include 'isSupplementTo', 'isSupplementedBy', 'merges,
	 * 'isMergedIn'.
	 */
	private String relClass;

	/**
	 * The source entity id.
	 */
	private String source;

	/**
	 * The target entity id.
	 */
	private String target;

	/**
	 * Was this relationship authoritatively validated?
	 */
	private Boolean validated;

	/**
	 * When was this relationship authoritatively validated.
	 */
	private String validationDate;

	/**
	 * List of relation specific properties. Values include 'similarityLevel', indicating the similarity score between a
	 * pair of publications.
	 */
	private List<KeyValue> properties = new ArrayList<>();

	public String getRelType() {
		return relType;
	}

	public void setRelType(final String relType) {
		this.relType = relType;
	}

	public String getSubRelType() {
		return subRelType;
	}

	public void setSubRelType(final String subRelType) {
		this.subRelType = subRelType;
	}

	public String getRelClass() {
		return relClass;
	}

	public void setRelClass(final String relClass) {
		this.relClass = relClass;
	}

	public String getSource() {
		return source;
	}

	public void setSource(final String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(final String target) {
		this.target = target;
	}

	public List<KeyValue> getProperties() {
		return properties;
	}

	public void setProperties(List<KeyValue> properties) {
		this.properties = properties;
	}

	public Boolean getValidated() {
		return Objects.nonNull(validated) && validated;
	}

	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	public String getValidationDate() {
		return validationDate;
	}

	public void setValidationDate(String validationDate) {
		this.validationDate = validationDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getRelType(), getSubRelType(), getRelClass(), getSource(), getTarget());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Relation relation = (Relation) o;
		return Objects.equals(getRelType(), relation.getRelType()) &&
				Objects.equals(getSubRelType(), relation.getSubRelType()) &&
				Objects.equals(getRelClass(), relation.getRelClass()) &&
				Objects.equals(getSource(), relation.getSource()) &&
				Objects.equals(getTarget(), relation.getTarget());
	}

}
