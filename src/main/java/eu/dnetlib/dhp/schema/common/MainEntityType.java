
package eu.dnetlib.dhp.schema.common;

/** Main entity types in the Graph */
public enum MainEntityType {
	result, datasource, organization, project, person
}
