package eu.dnetlib.dhp.schema.sx.api.model.v1;

import static eu.dnetlib.dhp.schema.sx.api.model.v2.RelationshipType.relationMapping;

import java.util.Objects;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

public class ScholixRelationship {

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("schema")
    private String schema = null;

    @NotBlank
    @JsonProperty("inverseRelationship")
    private String inverseRelationship = null;

    public ScholixRelationship name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     * @return name
     **/
    @Schema(required = true, description = "The relationship type chosen from a Scholix controlled vocabulary")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ScholixRelationship schema(String schema) {
        this.schema = schema;
        return this;
    }

    /**
     * Get The name of the schema or controlled list from which Relationship Sub-type is sourced
     * @return subType
     **/
    @Schema(description = "The name of the schema or controlled list from which Relationship Sub-type is sourced")
    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public ScholixRelationship inverseRelationship(String inverseRelationship) {
        this.inverseRelationship = inverseRelationship;
        return this;
    }

    /**
     * Get inverseRelationship
     * @return inverseRelationship
     **/
    @Schema(description = "The value of the inverse relation")

    public String getInverseRelationship() {
        return inverseRelationship;
    }

    public void setInverseRelationship(String inverseRelationship) {
        this.inverseRelationship = inverseRelationship;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ScholixRelationship relationshipType = (ScholixRelationship) o;
        return Objects.equals(this.name, relationshipType.name) &&
                Objects.equals(this.schema, relationshipType.schema) &&
                Objects.equals(this.inverseRelationship, relationshipType.inverseRelationship);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, schema, inverseRelationship);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class RelationshipType {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    subType: ").append(toIndentedString(schema)).append("\n");
        sb.append("    subTypeSchema: ").append(toIndentedString(inverseRelationship)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static ScholixRelationship fromScholixIndexRelationship(eu.dnetlib.dhp.schema.sx.scholix.ScholixRelationship inputRel) {
        if(inputRel==null)
            return null;

        ScholixRelationship result = new ScholixRelationship();

        result.setName(relationMapping.getOrDefault(inputRel.getName(), "IsRelatedTo"));
        result.setInverseRelationship(relationMapping.getOrDefault(inputRel.getInverse(), "IsRelatedTo"));
        result.setSchema(inputRel.getSchema());
        return result;

    }

}
