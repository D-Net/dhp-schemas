package eu.dnetlib.dhp.schema.sx.api.model.v2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixEntityId;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The type Scholix link provider type.
 */
public class ScholixLinkProviderType {
    @NotBlank
    @JsonProperty("name")
    private String name = null;


    @NotBlank
    @JsonProperty("identifier")
    private List<ScholixIdentifierType> identifier = new ArrayList<>();

    /**
     * Gets A List of unique string that identifies the object
     *
     * @return the identifier
     */
    @Schema(description = "A List of unique string that identifies the object")
    public List<ScholixIdentifierType> getIdentifier() {
        return identifier;
    }

    /**
     * Sets A List of unique string that identifies the object
     *
     * @param identifier the identifier
     * @return the identifier
     */
    public ScholixLinkProviderType setIdentifier(List<ScholixIdentifierType> identifier) {
        this.identifier = identifier;
        return this;
    }


    /**
     * Gets The name of the Provider
     *
     * @return the name
     */
    @Schema(description = "The name of the Provider")
    public String getName() {
        return name;
    }

    /**
     * Sets The name of the Provider
     *
     * @param name the name
     * @return the name
     */
    public ScholixLinkProviderType setName(String name) {
        this.name = name;
        return this;
    }


    public static ScholixLinkProviderType fromScholixEntityId(final ScholixEntityId provider) {
        if (provider == null)
            return null;
        ScholixLinkProviderType instance = new ScholixLinkProviderType().setName(provider.getName());


        if (provider.getIdentifiers()!= null && provider.getIdentifiers().size()>0)
            instance.setIdentifier(provider.getIdentifiers()
                    .stream()
                    .map(i ->new ScholixIdentifierType()
                                    .setId(i.getIdentifier())
                                    .setIdScheme(i.getSchema()))
                    .collect(Collectors.toList()));
        return instance;


    }
}
