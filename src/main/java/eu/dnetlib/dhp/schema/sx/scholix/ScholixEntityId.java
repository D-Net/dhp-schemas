
package eu.dnetlib.dhp.schema.sx.scholix;

import static eu.dnetlib.dhp.schema.sx.scholix.ScholixComparator.normalizeString;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Iterators;

public class ScholixEntityId implements Serializable, Comparable<ScholixEntityId> {
	private String name;
	private List<ScholixIdentifier> identifiers;

	public ScholixEntityId() {
	}

	public ScholixEntityId(String name, List<ScholixIdentifier> identifiers) {
		this.name = name;
		this.identifiers = identifiers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ScholixIdentifier> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(List<ScholixIdentifier> identifiers) {
		this.identifiers = identifiers;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ScholixEntityId that = (ScholixEntityId) o;
		return compareTo(that)==0;
	}

	@Override
	public int hashCode() {
		if (identifiers != null)
			return Objects.hash(normalizeString(name), identifiers.stream().sorted().collect(Collectors.toList()));
		else
			return Objects.hash(normalizeString(name));
	}

	@Override
	public int compareTo(ScholixEntityId other) {
		if (other == null)
			return -1;
		final int nameComp = StringUtils.compare(normalizeString(name), normalizeString(other.getName()));
		if (nameComp != 0)
			return nameComp;
		return ScholixComparator.compareList(identifiers,other.getIdentifiers());

	}
}
