package eu.dnetlib.dhp.schema.sx.api.model.v2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The main class that show the page result from the method
 * /l2/links
 */
public class PageResultType implements Serializable {

    @NotBlank
    @JsonProperty("currentPage")
    private Integer currentPage = 0;

    @NotBlank
    @JsonProperty("totalLinks")
    private  Integer totalLinks= 0;

    @NotBlank
    @JsonProperty("totalPages")
    private Integer totalPages = 0;

    @NotBlank
    @JsonProperty("result")
    private List<ScholixType> result = new ArrayList<>();


    /**
     * Gets current page.
     *
     * @return the current page
     */
    @Schema(description = "The Current page of the results")
    public Integer getCurrentPage() {
        return currentPage;
    }

    /**
     * Sets current page.
     *
     * @param currentPage the current page
     */
    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Gets total links.
     *
     * @return the total links
     */
    @Schema(description = "The total number of Links found by the query")
    public Integer getTotalLinks() {
        return totalLinks;
    }

    /**
     * Sets total links.
     *
     * @param totalLinks the total links
     */
    public void setTotalLinks(Integer totalLinks) {
        this.totalLinks = totalLinks;
    }

    /**
     * Gets total pages.
     *
     * @return the total pages
     */
    @Schema(description = "The Total number of pages")
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets total pages.
     *
     * @param totalPages the total pages
     */
    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    @Schema(description = "The First page of Scholix results")
    public List<ScholixType> getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result the result
     */
    public void setResult(List<ScholixType> result) {
        this.result = result;
    }
}
