package eu.dnetlib.dhp.schema.sx.api.model.v2;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixIdentifier;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The type Scholix identifier type.
 */
public class ScholixIdentifierType {

    @NotBlank
    @JsonProperty("ID")
    private String id = null;

    @NotBlank
    @JsonProperty("IDScheme")
    private String idScheme = null;

    @NotBlank
    @JsonProperty("IDURL")
    private String idURL = null;


    /**
     * Gets The identifier
     *
     * @return the id
     */
    @Schema(description = "The identifier")
    public String getId() {
        return id;
    }

    /**
     * Sets The identifier
     *
     * @param id the id
     * @return the id
     */
    public ScholixIdentifierType setId(String id) {
        this.id = id;
        return this;
    }

    /**
     * Gets The scheme or namespace of the identifier string
     *
     * @return the id scheme
     */
    @Schema(description = "The scheme or namespace of the identifier string")
    public String getIdScheme() {
        return idScheme;
    }

    /**
     * Sets The scheme or namespace of the identifier string
     *
     * @param idScheme the id scheme
     * @return the id scheme
     */
    public ScholixIdentifierType setIdScheme(String idScheme) {
        this.idScheme = idScheme;
        return this;
    }

    /**
     * Gets An internet resolvable form of the identifier
     *
     * @return the id url
     */
    @Schema(description = "An internet resolvable form of the identifier")
    public String getIdURL() {
        return idURL;
    }

    /**
     * Sets An internet resolvable form of the identifier
     *
     * @param idURL the id url
     * @return the id url
     */
    public ScholixIdentifierType setIdURL(String idURL) {
        this.idURL = idURL;
        return this;
    }


    public static ScholixIdentifierType fromScholixIdentifier(ScholixIdentifier input) {
        if (input== null)
            return null;
        final ScholixIdentifierType instance = new ScholixIdentifierType();

        instance.setId(input.getIdentifier());
        instance.setIdScheme(input.getSchema());
        instance.setIdURL(input.getUrl());
        return instance;
    }



}
