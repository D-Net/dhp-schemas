package eu.dnetlib.dhp.schema.sx.api.model.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixResource;
import io.swagger.v3.oas.annotations.media.Schema;

public class ScholixItem {

    @JsonProperty("identifiers")
    private List<ScholixIdentifier> identifiers;

    @JsonProperty("objectType")
    private String objectType;

    @JsonProperty("objectSubType")
    private String objectSubType;

    @JsonProperty("title")
    private String title;

    @JsonProperty("creator")
    private List<ScholixCreator> creator;

    @JsonProperty("publicationDate")
    private String publicationDate;

    @JsonProperty("publisher")
    private List<ScholixProvider> publisher;

    @Schema(description = "The list of identifiers")
    public List<ScholixIdentifier> getIdentifiers() {
        return identifiers;
    }

    public ScholixItem setIdentifiers(List<ScholixIdentifier> identifiers) {
        this.identifiers = identifiers;
        return this;
    }

    public ScholixItem objectType(String objectType) {
        this.objectType = objectType;
        return  this;
    }

    @Schema(description = "Describes the nature of the object (its intended usage)")
    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public ScholixItem objectSubType(String objectSubType) {
        this.objectSubType = objectSubType;
        return this;
    }

    @Schema(description = "The sub-type of  Object")
    public String getObjectSubType() {
        return objectSubType;
    }

    public void setObjectSubType(String objectSubType) {
        this.objectSubType = objectSubType;
    }

    public ScholixItem title(String title) {
        this.title = title;
        return this;
    }

    @Schema(description = "The name of the object")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ScholixItem creator(List<ScholixCreator> creator) {
        this.creator = creator;
        return this;
    }

    public ScholixItem addCreatorInstance(ScholixCreator creatorInstance) {
        if(this.creator == null)
            this.creator = new ArrayList<>();
        this.creator.add(creatorInstance);
        return this;
    }

    @Schema(description = "Party responsible for the creation of the object")
    public List<ScholixCreator> getCreator() {
        return creator;
    }

    public void setCreator(List<ScholixCreator> creator) {
        this.creator = creator;
    }

    @Schema(description = "The date the object was formally issued, published or distributed")
    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Schema(description = "The list name of the publisher of the object")
    public List<ScholixProvider> getPublisher() {
        return publisher;
    }

    public ScholixItem setPublisher(List<ScholixProvider> publisher) {
        this.publisher = publisher;
        return this;
    }

    public static ScholixItem fromScholixResource(final ScholixResource input) {
        if (input == null)
            return null;


        final  ScholixItem result = new ScholixItem();

        if (input.getIdentifier()!= null)
        result.setIdentifiers(
                input.getIdentifier().stream()
                        .map(ScholixIdentifier::fromScholixIdentifier)
                        .collect(Collectors.toList())
        );

        result.setTitle(input.getTitle());
        result.setObjectType(input.getObjectType());
        result.setObjectSubType(input.getObjectSubType());
        result.setPublicationDate(input.getPublicationDate());
        if(input.getPublisher()!= null)
            result.setPublisher(input.getPublisher().stream()
                    .map(ScholixProvider::fromScholixEntityId)
                    .collect(Collectors.toList())
            );


        if (input.getCreator()!= null)
            result.setCreator(input.getCreator().stream()
                    .map(ScholixCreator::fromScholixEntityId)
                    .collect(Collectors.toList())
            );

        return result;

    }
}
