package eu.dnetlib.dhp.schema.sx.api.model.v2;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixRelationship;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The type Relationship type.
 */
public class RelationshipType {

    @NotNull
    @JsonProperty("Name")
    private String name;

    @JsonProperty("SubType")
    private String subType;

    @JsonProperty("SubTypeSchema")
    private String subTypeSchema;


    public static Map<String, String> relationMapping = Stream.of(new String[][] {
            { "issupplementto", "IsSupplementTo" },
            { "issupplementedby", "IsSupplementedBy" },
            { "references", "References" },
            { "isreferencedby", "IsReferencedBy" },
    }).collect(Collectors.toMap(data -> data[0], data -> data[1]));


    /**
     * Gets The relationship type chosen from a Scholix controlled vocabulary
     *
     * @return the name
     */
    @Schema(description = "The relationship type chosen from a Scholix controlled vocabulary")
    public String getName() {
        return name;
    }

    /**
     * Sets The relationship type chosen from a Scholix controlled vocabulary
     *
     * @param name the name
     * @return the RelationshipType instance
     */
    public RelationshipType setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Gets The sub-type of RelationshipType.Name
     *
     * @return the sub type
     */
    @Schema(description = "The sub-type of RelationshipType.Name")
    public String getSubType() {
        return subType;
    }

    /**
     * Sets The sub-type of RelationshipType.Name
     *
     * @param subType the sub type
     * @return the RelationshipType instance
     */
    public RelationshipType setSubType(String subType) {
        this.subType = subType;
        return this;
    }

    /**
     * Gets The name of the schema or controlled list from which RelationshipSub-type is sourced.
     *
     * @return the sub type schema
     */
    @Schema(description = "The name of the schema or controlled list from which RelationshipSub-type is sourced")
    public String getSubTypeSchema() {
        return subTypeSchema;
    }

    /**
     * Sets The name of the schema or controlled list from which RelationshipSub-type is sourced.
     *
     * @param subTypeSchema the sub type schema
     * @return the RelationshipType instance
     */
    public RelationshipType setSubTypeSchema(String subTypeSchema) {
        this.subTypeSchema = subTypeSchema;
        return this;
    }

    public static RelationshipType fromScholixRelationship(ScholixRelationship inputRels) {

        return new RelationshipType()
                .setName(relationMapping.getOrDefault(inputRels.getName(), "IsRelatedTo"))
                .setSubType(inputRels.getName())
                .setSubTypeSchema(inputRels.getSchema());

    }
}
