
package eu.dnetlib.dhp.schema.sx.scholix;

import static eu.dnetlib.dhp.schema.sx.scholix.ScholixComparator.*;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class ScholixRelationship implements Serializable, Comparable<ScholixRelationship> {
	private String name;
	private String schema;
	private String inverse;

	public ScholixRelationship() {
	}

	public ScholixRelationship(String name, String schema, String inverse) {
		this.name = name;
		this.schema = schema;
		this.inverse = inverse;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getInverse() {
		return inverse;
	}

	public void setInverse(String inverse) {
		this.inverse = inverse;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ScholixRelationship)) return false;
		ScholixRelationship that = (ScholixRelationship) o;
		return this.compareTo(that) ==0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(normalizeString(getName()), normalizeString(getSchema()), normalizeString(getInverse()));
	}

	@Override
	public int compareTo(ScholixRelationship other) {
		if (other == null)
			return -1;

		final int nameCompare = StringUtils.compare(normalizeString(name), normalizeString(other.getName()));
		if (nameCompare!= 0 )
			return nameCompare;

		final int schemaCompare = StringUtils.compare(normalizeString(schema), normalizeString(other.getSchema()));
		if (schemaCompare!= 0 )
			return schemaCompare;

		return StringUtils.compare(normalizeString(inverse), normalizeString(other.getInverse()));
	}
}
