package eu.dnetlib.dhp.schema.sx.api.model.v2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixEntityId;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The type Scholix creator type.
 */
public class ScholixCreatorType {

    @NotBlank
    @JsonProperty("name")
    private String name = null;


    @NotBlank
    @JsonProperty("identifier")
    private List<ScholixIdentifierType> identifier = new ArrayList<>();

    /**
     * Gets A List of unique string that identifies the creator
     *
     * @return the identifier
     */
    @Schema(description = "A List of unique string that identifies the creator")
    public List<ScholixIdentifierType> getIdentifier() {
        return identifier;
    }

    /**
     * Sets A List of unique string that identifies the creator
     *
     * @param identifier the identifier
     * @return the identifier
     */
    public ScholixCreatorType setIdentifier(List<ScholixIdentifierType> identifier) {
        this.identifier = identifier;
        return this;
    }


    /**
     * Gets The name of the Object Creator
     *
     * @return the name
     */
    @Schema(description = "The name of the Object Creator")
    public String getName() {
        return name;
    }

    /**
     * Sets The name of the Object Creator
     *
     * @param name the name
     * @return the name
     */
    public ScholixCreatorType setName(String name) {
        this.name = name;
        return this;
    }

    public static ScholixCreatorType fromScholixEntityId(final ScholixEntityId inputCreator) {
        if (inputCreator == null)
            return null;
        ScholixCreatorType instance = new ScholixCreatorType().setName(inputCreator.getName());


        if (inputCreator.getIdentifiers()!= null && inputCreator.getIdentifiers().size()>0)
            instance.setIdentifier(inputCreator.getIdentifiers()
                    .stream()
                    .map(i ->new ScholixIdentifierType()
                            .setId(i.getIdentifier())
                            .setIdScheme(i.getSchema()))
                    .collect(Collectors.toList()));
        return instance;


    }
}
