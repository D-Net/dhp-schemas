package eu.dnetlib.dhp.schema.sx.api.model.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixEntityId;
import io.swagger.v3.oas.annotations.media.Schema;

public class ScholixProvider {

    public static ScholixProvider fromScholixEntityId(final ScholixEntityId provider) {
        if (provider == null)
            return null;
        ScholixProvider instance = new ScholixProvider().setName(provider.getName());


        if (provider.getIdentifiers()!= null && provider.getIdentifiers().size()>0)
            instance.setIdentifier(provider.getIdentifiers()
                    .stream()
                    .map(i ->new ScholixIdentifier()
                            .setIdentifier(i.getIdentifier())
                            .setSchema(i.getSchema()))
                    .collect(Collectors.toList()));
        return instance;


    }


    @JsonProperty("name")
    private String name = null;

    @JsonProperty("identifier")
    @Valid
    private List<ScholixIdentifier> identifier = null;

    public ScholixProvider name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get the provider name
     * @return name
     **/
    @Schema(description = "The provider name")

    public String getName() {
        return name;
    }

    public ScholixProvider setName(String name) {
        this.name = name;
        return this;
    }

    public ScholixProvider identifier(List<ScholixIdentifier> identifier) {
        this.identifier = identifier;
        return this;
    }

    public ScholixProvider addIdentifierItem(ScholixIdentifier identifierItem) {
        if (this.identifier == null) {
            this.identifier = new ArrayList<>();
        }
        this.identifier.add(identifierItem);
        return this;
    }

    /**
     * Get identifier
     * @return identifier
     **/
    @Schema(description = "the identifiers of the provider")
    @Valid
    public List<ScholixIdentifier> getIdentifier() {
        return identifier;
    }

    public ScholixProvider setIdentifier(List<ScholixIdentifier> identifier) {
        this.identifier = identifier;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ScholixProvider scholixProviderType = (ScholixProvider) o;
        return Objects.equals(this.name, scholixProviderType.name) &&
                Objects.equals(this.identifier, scholixProviderType.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, identifier);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ScholixProviderType {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    identifiers: ").append(toIndentedString(identifier)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
