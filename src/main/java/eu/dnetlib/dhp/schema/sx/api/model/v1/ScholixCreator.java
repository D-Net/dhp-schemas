package eu.dnetlib.dhp.schema.sx.api.model.v1;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixEntityId;
import io.swagger.v3.oas.annotations.media.Schema;

public class ScholixCreator {

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("identifier")
    private List<ScholixIdentifier> identifier = null;

    public ScholixCreator name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     * @return name
     **/
    @Schema(required = true, description = "The name of the Object Creator")
    public String getName() {
        return name;
    }

    public ScholixCreator setName(String name) {
        this.name = name;
        return this;
    }


    /**
     * Get identifier
     * @return identifier
     **/
    @Schema(description = "A unique string that identifies the Object Creator")
    public List<ScholixIdentifier> getIdentifier() {
        return identifier;
    }

    public ScholixCreator setIdentifier(List<ScholixIdentifier> identifier) {
        this.identifier = identifier;
        return this;
    }

    public static ScholixCreator fromScholixEntityId(final ScholixEntityId provider) {
        if (provider == null)
            return null;
        ScholixCreator instance = new ScholixCreator().setName(provider.getName());


        if (provider.getIdentifiers()!= null && provider.getIdentifiers().size()>0)
            instance.setIdentifier(provider.getIdentifiers()
                    .stream()
                    .map(i ->new ScholixIdentifier()
                            .setIdentifier(i.getIdentifier())
                            .setSchema(i.getSchema()))
                    .collect(Collectors.toList()));
        return instance;


    }
}
