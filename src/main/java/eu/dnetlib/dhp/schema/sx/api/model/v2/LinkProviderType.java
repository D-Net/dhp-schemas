package eu.dnetlib.dhp.schema.sx.api.model.v2;

import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The type Link provider type.
 */
public class LinkProviderType {
    @NotBlank
    @JsonProperty("name")
    private String name = null;

    @NotBlank
    @JsonProperty("totalRelationships")
    private Integer totalRelationships = null;

    /**
     * Name link provider type.
     *
     * @param name the name
     * @return the link provider type
     */
    public LinkProviderType name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name name
     */
    @Schema(description = "The Publisher Name")
    @Size(max=300)
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Total relationships link provider type.
     *
     * @param totalRelationships the total relationships
     * @return the link provider type
     */
    public LinkProviderType totalRelationships(Integer totalRelationships) {
        this.totalRelationships = totalRelationships;
        return this;
    }

    /**
     * Get totalRelationships
     *
     * @return totalRelationships total relationships
     */
    @Schema(description = "Total number of relationships that the publisher provides")
    public Integer getTotalRelationships() {
        return totalRelationships;
    }

    /**
     * Sets total relationships.
     *
     * @param totalRelationships the total relationships
     */
    public void setTotalRelationships(Integer totalRelationships) {
        this.totalRelationships = totalRelationships;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LinkProviderType linkPublisher = (LinkProviderType) o;
        return Objects.equals(this.name, linkPublisher.name) &&
                Objects.equals(this.totalRelationships, linkPublisher.totalRelationships);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, totalRelationships);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class LinkPublisher {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    totalRelationships: ").append(toIndentedString(totalRelationships)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
