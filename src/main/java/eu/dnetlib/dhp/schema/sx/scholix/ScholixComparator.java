package eu.dnetlib.dhp.schema.sx.scholix;

import java.text.Normalizer;
import java.util.List;
import java.util.stream.Stream;

import com.google.common.collect.Iterators;

import me.xuender.unidecode.Unidecode;

public class ScholixComparator {


    public static String normalizeIdnetifier(final String input) {
        if (input == null)
            return null;

        return Normalizer.normalize(input, Normalizer.Form.NFD)
                .toLowerCase();
    }


    public static String normalizeString(final String input) {
        if (input == null)
            return null;
        return Unidecode.decode(input).toLowerCase();
    }


    public static <T extends Comparable<T>> int compareObjects (T left, T right) {
        if (left == null && right==null)
            return 0;
        if(left == null)
            return 1;
        if (right == null)
            return -1;
        return left.compareTo(right);
    }

    public static <T extends Comparable<T>> int compareList (List<T> left, List<T> right) {

        if (left == null && right==null)
            return 0;
        if(left == null)
            return 1;
        if (right == null)
            return -1;

        Stream<T> sortedLeft = left.stream().sorted();
        Stream<T> sortedRight = right.stream().sorted();
        boolean equals = Iterators.elementsEqual(sortedLeft.iterator(), sortedRight.iterator());

        return equals? 0: -1;


    }


}
