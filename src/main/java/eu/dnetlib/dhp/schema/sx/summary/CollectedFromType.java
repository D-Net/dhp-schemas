
package eu.dnetlib.dhp.schema.sx.summary;

import java.io.Serializable;
import java.util.Objects;

public class CollectedFromType implements Serializable {

	private String datasourceName;
	private String datasourceId;
	private String completionStatus;

	public CollectedFromType() {
	}

	public CollectedFromType(String datasourceName, String datasourceId, String completionStatus) {
		this.datasourceName = datasourceName;
		this.datasourceId = datasourceId;
		this.completionStatus = completionStatus;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CollectedFromType that = (CollectedFromType) o;
		return Objects.equals(datasourceName, that.datasourceName) && Objects.equals(datasourceId, that.datasourceId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(datasourceName, datasourceId);
	}

	public String getDatasourceName() {
		return datasourceName;
	}

	public void setDatasourceName(String datasourceName) {
		this.datasourceName = datasourceName;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}
}
