
package eu.dnetlib.dhp.schema.sx.scholix;

import static eu.dnetlib.dhp.schema.sx.scholix.ScholixComparator.normalizeIdnetifier;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

/**
 * The type Scholix identifier.
 */
public class ScholixIdentifier implements Serializable, Comparable<ScholixIdentifier> {
	private String identifier;
	private String schema;
	private String url;

	/**
	 * Instantiates a new Scholix identifier.
	 */
	public ScholixIdentifier() {
	}

	/**
	 * Instantiates a new Scholix identifier.
	 *
	 * @param identifier the identifier
	 * @param schema     the schema
	 * @param url        the url
	 */
	public ScholixIdentifier(String identifier, String schema, String url) {
		this.identifier = identifier;
		this.schema = schema;
		this.url = url;
	}


	/**
	 * Gets url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets url.
	 *
	 * @param url the url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Sets identifier.
	 *
	 * @param identifier the identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Gets schema.
	 *
	 * @return the schema
	 */
	public String getSchema() {
		return schema;
	}

	/**
	 * Sets schema.
	 *
	 * @param schema the schema
	 */
	public void setSchema(String schema) {
		this.schema = schema;
	}

	@Override
	public int compareTo(ScholixIdentifier other) {
		if (other == null)
			return -1;
		final int idComp = StringUtils.compare(normalizeIdnetifier(identifier), normalizeIdnetifier(other.getIdentifier()));
		if (idComp !=0)
			return idComp;
		final int schemaComp = StringUtils.compare(normalizeIdnetifier(schema), normalizeIdnetifier(other.getSchema()));
		if (schemaComp !=0)
			return schemaComp;
		return StringUtils.compare(normalizeIdnetifier(url), normalizeIdnetifier(other.getUrl()));
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ScholixIdentifier that = (ScholixIdentifier) o;
		return compareTo(that) == 0;
	}


	@Override
	public int hashCode() {
		return Objects.hash(normalizeIdnetifier(identifier), normalizeIdnetifier(schema), normalizeIdnetifier(url));
	}


}
