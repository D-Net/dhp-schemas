package eu.dnetlib.dhp.schema.sx.api.model.v3;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.api.model.v2.RelationshipType;
import eu.dnetlib.dhp.schema.sx.api.model.v2.ScholixLinkProviderType;
import eu.dnetlib.dhp.schema.sx.scholix.Scholix;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The type Scholix type.
 */
public class ScholixType implements Serializable {


    @NotBlank
    @JsonProperty("RelationshipType")
    private RelationshipType relationshipType ;

    @NotBlank
    @JsonProperty("source")
    private ScholixItemType source ;

    @NotBlank
    @JsonProperty("target")
    private ScholixItemType target ;

    @JsonProperty("HarvestDate")
    private String harvestDate ;


    @JsonProperty("LicenseURL")
    private String licenseURL ;

    @JsonProperty("LinkProvider")
    private List<ScholixLinkProviderType> linkProvider ;

    @JsonProperty("LinkPublicationDate")
    private String linkPublicationDate ;


    /**
     * Gets the nature of the relationship between the source object and target object in this Link Information Package
     *
     * @return the relationship type
     */
    @Schema(description = "The nature of the relationship between the source object and target object in this Link Information Package")
    public RelationshipType getRelationshipType() {
        return relationshipType;
    }

    /**
     * Sets relationship type.
     *
     * @param relationshipType the relationship type
     */
    public void setRelationshipType(RelationshipType relationshipType) {
        this.relationshipType = relationshipType;
    }

    /**
     * Gets Root element relative to all properties describing the link’s source object
     *
     * @return the source
     */
    @Schema(description = "Root element relative to all properties describing the link’s source object")
    public ScholixItemType getSource() {
        return source;
    }

    /**
     * Sets source.
     *
     * @param source the source
     */
    public void setSource(ScholixItemType source) {
        this.source = source;
    }

    /**
     * Gets Root element relative to all properties describing the link’s target object
     *
     * @return the target
     */
    @Schema(description = "Gets Root element relative to all properties describing the link’s target object")
    public ScholixItemType getTarget() {
        return target;
    }

    /**
     * Sets target.
     *
     * @param target the target
     */
    public void setTarget(ScholixItemType target) {
        this.target = target;
    }

    /**
     * Gets harvest date.
     *
     * @return the harvest date
     */
    public String getHarvestDate() {
        return harvestDate;
    }

    /**
     * Sets harvest date.
     *
     * @param harvestDate the harvest date
     */
    public void setHarvestDate(String harvestDate) {
        this.harvestDate = harvestDate;
    }

    /**
     * Gets The URL of the license for the Scholix Link Information Package
     *
     * @return the license url
     */
    @Schema(description = "The URL of the license for the Scholix Link Information Package")
    public String getLicenseURL() {
        return licenseURL;
    }

    /**
     * Sets license url.
     *
     * @param licenseURL the license url
     */
    public void setLicenseURL(String licenseURL) {
        this.licenseURL = licenseURL;
    }

    /**
     * Gets The source(s) of this Link Information Package.
     *
     * @return the link provider
     */
    @Schema(description = "The source(s) of this Link Information Package")
    public List<ScholixLinkProviderType> getLinkProvider() {
        return linkProvider;
    }

    /**
     * Sets link provider.
     *
     * @param linkProvider the link provider
     */
    public void setLinkProvider(List<ScholixLinkProviderType> linkProvider) {
        this.linkProvider = linkProvider;
    }

    /**
     * Gets Date when this Link Information Package was first formally issued from this current Provider
     *
     * @return the link publication date
     */
    @Schema(description = "Date when this Link Information Package was first formally issued from this current Provider")

    public String getLinkPublicationDate() {
        return linkPublicationDate;
    }

    /**
     * Sets link publication date.
     *
     * @param linkPublicationDate the link publication date
     */
    public void setLinkPublicationDate(String linkPublicationDate) {
        this.linkPublicationDate = linkPublicationDate;
    }



    public static ScholixType fromScholix(Scholix input) {
        final ScholixType instance = new ScholixType();
        instance.setLinkPublicationDate(input.getPublicationDate());
        instance.setHarvestDate(input.getPublicationDate());
        instance.setRelationshipType(RelationshipType.fromScholixRelationship(input.getRelationship()));

        if(input.getLinkprovider()!= null && input.getLinkprovider().size()>0)
            instance.setLinkProvider(input.getLinkprovider()
                    .stream()
                    .map(ScholixLinkProviderType::fromScholixEntityId)
                    .collect(Collectors.toList())
            );


        instance.setSource(ScholixItemType.fromScholixResource(input.getSource()));
        instance.setTarget(ScholixItemType.fromScholixResource(input.getTarget()));
        return instance;
    }
}
