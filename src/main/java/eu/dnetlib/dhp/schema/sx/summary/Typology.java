
package eu.dnetlib.dhp.schema.sx.summary;

import java.io.Serializable;

public enum Typology implements Serializable {
	dataset, publication, unknown
}
