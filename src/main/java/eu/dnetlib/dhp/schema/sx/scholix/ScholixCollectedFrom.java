
package eu.dnetlib.dhp.schema.sx.scholix;

import static eu.dnetlib.dhp.schema.sx.scholix.ScholixComparator.*;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class ScholixCollectedFrom implements Serializable, Comparable<ScholixCollectedFrom> {

	private ScholixEntityId provider;
	private String provisionMode;
	private String completionStatus;

	public ScholixCollectedFrom() {
	}

	public ScholixCollectedFrom(
            ScholixEntityId provider, String provisionMode, String completionStatus) {
		this.provider = provider;
		this.provisionMode = provisionMode;
		this.completionStatus = completionStatus;
	}

	public ScholixEntityId getProvider() {
		return provider;
	}

	public void setProvider(ScholixEntityId provider) {
		this.provider = provider;
	}

	public String getProvisionMode() {
		return provisionMode;
	}

	public void setProvisionMode(String provisionMode) {
		this.provisionMode = provisionMode;
	}

	public String getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ScholixCollectedFrom)) return false;
		ScholixCollectedFrom that = (ScholixCollectedFrom) o;
		return compareTo(that)==0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(provider, normalizeString(provisionMode), normalizeString(completionStatus));
	}

	@Override
	public int compareTo(ScholixCollectedFrom other) {
		if (other == null)
			return -1;

		int provModeCompare = StringUtils.compare(normalizeString(provisionMode),normalizeString(other.getProvisionMode()) );
		int compStatusCompare =StringUtils.compare(normalizeString(completionStatus),normalizeString(other.getCompletionStatus()) );

		if (provider == null && other.getProvider() == null)
			return provModeCompare == 0 ? compStatusCompare: provModeCompare;


		if (provider == null)
			return 1;
		if (other.getProvider() == null)
			return -1;

		int provCompare = provider.compareTo(other.getProvider());

		if (provCompare == 0)
			return provModeCompare == 0 ? compStatusCompare: provModeCompare;
		return provCompare;
	}
}
