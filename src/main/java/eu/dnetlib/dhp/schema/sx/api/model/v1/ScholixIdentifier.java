package eu.dnetlib.dhp.schema.sx.api.model.v1;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

public class ScholixIdentifier {

    @JsonProperty("identifier")
    private String identifier = null;

    @JsonProperty("schema")
    private String schema = null;



    public ScholixIdentifier Identifier(String ID) {
        this.identifier = ID;
        return this;
    }

    /**
     * Get ID
     * @return ID
     **/
    @Schema(description = "The value of the Identifier")
    public String getIdentifier() {
        return identifier;
    }

    public ScholixIdentifier setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public ScholixIdentifier Schema(String idScheme) {
        this.schema = idScheme;
        return this;
    }

    /**
     * Get the Schema
     * @return Schema
     **/
    @Schema(description = "The Schema URL of the identifier type")

    public String getSchema() {
        return schema;
    }

    public ScholixIdentifier setSchema(String schema) {
        this.schema = schema;
        return this;
    }

    public static ScholixIdentifier fromScholixIdentifier(eu.dnetlib.dhp.schema.sx.scholix.ScholixIdentifier input) {
        if (input == null)
            return null;

        final ScholixIdentifier result = new ScholixIdentifier();
        result.setSchema(input.getSchema());
        result.setIdentifier(input.getIdentifier());
        return result;



    }


}
