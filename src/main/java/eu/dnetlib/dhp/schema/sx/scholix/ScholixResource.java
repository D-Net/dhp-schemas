
package eu.dnetlib.dhp.schema.sx.scholix;

import static eu.dnetlib.dhp.schema.sx.scholix.ScholixComparator.*;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class ScholixResource implements Serializable, Comparable<ScholixResource> {

	private List<ScholixIdentifier> identifier;
	private String dnetIdentifier;
	private String objectType;
	private String objectSubType;
	private String title;
	private List<ScholixEntityId> creator;
	private String publicationDate;
	private List<ScholixEntityId> publisher;
	private List<ScholixCollectedFrom> collectedFrom;

	public List<ScholixIdentifier> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(List<ScholixIdentifier> identifier) {
		this.identifier = identifier;
	}

	public String getDnetIdentifier() {
		return dnetIdentifier;
	}

	public void setDnetIdentifier(String dnetIdentifier) {
		this.dnetIdentifier = dnetIdentifier;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectSubType() {
		return objectSubType;
	}

	public void setObjectSubType(String objectSubType) {
		this.objectSubType = objectSubType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ScholixEntityId> getCreator() {
		return creator;
	}

	public void setCreator(List<ScholixEntityId> creator) {
		this.creator = creator;
	}

	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	public List<ScholixEntityId> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<ScholixEntityId> publisher) {
		this.publisher = publisher;
	}

	public List<ScholixCollectedFrom> getCollectedFrom() {
		return collectedFrom;
	}

	public void setCollectedFrom(List<ScholixCollectedFrom> collectedFrom) {
		this.collectedFrom = collectedFrom;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ScholixResource)) return false;
		ScholixResource that = (ScholixResource) o;
		return compareTo(that) == 0;
	}

	@Override
	public int hashCode() {
		int idHash = identifier == null ? 0 : identifier.stream().sorted().collect(Collectors.toList()).hashCode();
		int creatorHash = creator == null ? 0 : creator.stream().sorted().collect(Collectors.toList()).hashCode();

		int publisherHash = publisher == null ? 0 : publisher.stream().sorted().collect(Collectors.toList()).hashCode();
		int collectedFromHash = collectedFrom == null ? 0 : collectedFrom.stream().sorted().collect(Collectors.toList()).hashCode();

		return Objects.hash(idHash, normalizeIdnetifier(dnetIdentifier), normalizeString(objectType),
				normalizeString(objectSubType), normalizeString(title),creatorHash, normalizeString(publicationDate), publisherHash, collectedFromHash);
	}

	@Override
	public int compareTo(ScholixResource other) {
		if (other == null)
			return -1;
		final int compIdentifiers = compareList(identifier, other.getIdentifier());
		if (compIdentifiers!= 0)
			return compIdentifiers;

		final int dnetIdComp = StringUtils.compare(dnetIdentifier, other.getDnetIdentifier());

		if (dnetIdComp != 0)
			return dnetIdComp;

		final int objTypeComparator = StringUtils.compare(normalizeString(objectType), normalizeString(other.getObjectType()));

		if (objTypeComparator != 0)
			return objTypeComparator;


		final int objSubTypeComparator = StringUtils.compare(normalizeString(objectSubType), normalizeString(other.getObjectSubType()));

		if (objSubTypeComparator != 0)
			return objSubTypeComparator;


		final int titleComparator = StringUtils.compare(normalizeString(title), normalizeString(other.getTitle()));

		if (titleComparator != 0)
			return titleComparator;

		final int creatorComparator = compareList(creator, other.getCreator());
		if (creatorComparator!= 0)
			return creatorComparator;

		final int pubDateComparator = StringUtils.compare(normalizeString(publicationDate), normalizeString(other.getPublicationDate()));
		if (pubDateComparator!= 0)
			return pubDateComparator;

		final int publisherComparator = compareList(publisher, other.getPublisher());
		if (publisherComparator!= 0)
			return publisherComparator;

		return compareList(collectedFrom, other.getCollectedFrom());

	}
}
