
package eu.dnetlib.dhp.schema.sx.scholix;

import static eu.dnetlib.dhp.schema.sx.scholix.ScholixComparator.*;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class Scholix implements Serializable, Comparable<Scholix> {

	private String publicationDate;

	private List<ScholixEntityId> publisher;

	private List<ScholixEntityId> linkprovider;

	private ScholixRelationship relationship;

	private ScholixResource source;

	private ScholixResource target;

	private String identifier;

	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	public List<ScholixEntityId> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<ScholixEntityId> publisher) {
		this.publisher = publisher;
	}

	public List<ScholixEntityId> getLinkprovider() {
		return linkprovider;
	}

	public void setLinkprovider(List<ScholixEntityId> linkprovider) {
		this.linkprovider = linkprovider;
	}

	public ScholixRelationship getRelationship() {
		return relationship;
	}

	public void setRelationship(ScholixRelationship relationship) {
		this.relationship = relationship;
	}

	public ScholixResource getSource() {
		return source;
	}

	public void setSource(ScholixResource source) {
		this.source = source;
	}

	public ScholixResource getTarget() {
		return target;
	}

	public void setTarget(ScholixResource target) {
		this.target = target;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Scholix)) return false;
		Scholix scholix = (Scholix) o;
		return compareTo(scholix) == 0;
	}

	@Override
	public int hashCode() {
		final int publisherHash = publisher == null ? 0: publisher.stream().sorted().collect(Collectors.toList()).hashCode();
		final int linkProviderHash = linkprovider == null ? 0: linkprovider.stream().sorted().collect(Collectors.toList()).hashCode();

		return Objects.hash(normalizeString(publicationDate),publisherHash, linkProviderHash, relationship, source, target, normalizeIdnetifier(identifier));
	}

	@Override
	public int compareTo(Scholix other) {

		if (other == null)
			return  -1;

		// Comparing publication date
		final int publicationDateCompare =StringUtils.compare(publicationDate, other.getPublicationDate());

		if (publicationDateCompare != 0)
			return publicationDateCompare;

		final int linkPublisherComparator = compareList(publisher, other.getPublisher());
		if (linkPublisherComparator!= 0)
			return linkPublisherComparator;

		final int linkProviderComparator = compareList(linkprovider, other.getLinkprovider());
		if (linkProviderComparator!= 0)
			return linkProviderComparator;


		final int relsComparator = compareObjects(relationship, other.getRelationship());
		if (relsComparator!= 0)
			return relsComparator;


		final int sourceComparator = compareObjects(source, other.getSource());
		if (sourceComparator!= 0)
			return sourceComparator;

		return compareObjects(target, other.getTarget());
	}
}
