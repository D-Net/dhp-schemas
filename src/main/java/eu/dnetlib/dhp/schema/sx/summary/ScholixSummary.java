
package eu.dnetlib.dhp.schema.sx.summary;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.ScholixIdentifier;

public class ScholixSummary implements Serializable {
	private String id;
	private List<ScholixIdentifier> localIdentifier;
	private Typology typology;
	private String subType;
	private List<String> title;
	private List<String> author;
	private List<String> date;
	private String description;
	private List<SchemeValue> subject;
	private List<String> publisher;
	private long relatedPublications;
	private long relatedDatasets;
	private long relatedUnknown;
	private List<CollectedFromType> datasources;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<ScholixIdentifier> getLocalIdentifier() {
		return localIdentifier;
	}

	public void setLocalIdentifier(List<ScholixIdentifier> localIdentifier) {
		this.localIdentifier = localIdentifier;
	}

	public Typology getTypology() {
		return typology;
	}

	public void setTypology(Typology typology) {
		this.typology = typology;
	}

	public List<String> getTitle() {
		return title;
	}

	public void setTitle(List<String> title) {
		this.title = title;
	}

	public List<String> getAuthor() {
		return author;
	}

	public void setAuthor(List<String> author) {
		this.author = author;
	}

	public List<String> getDate() {
		return date;
	}

	public void setDate(List<String> date) {
		this.date = date;
	}

	@JsonProperty("abstract")
	public String getDescription() {
		return description;
	}

	@JsonProperty("abstract")
	public void setDescription(String description) {
		this.description = description;
	}

	public List<SchemeValue> getSubject() {
		return subject;
	}

	public void setSubject(List<SchemeValue> subject) {
		this.subject = subject;
	}

	public List<String> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<String> publisher) {
		this.publisher = publisher;
	}

	public long getRelatedPublications() {
		return relatedPublications;
	}

	public void setRelatedPublications(long relatedPublications) {
		this.relatedPublications = relatedPublications;
	}

	public long getRelatedDatasets() {
		return relatedDatasets;
	}

	public void setRelatedDatasets(long relatedDatasets) {
		this.relatedDatasets = relatedDatasets;
	}

	public long getRelatedUnknown() {
		return relatedUnknown;
	}

	public void setRelatedUnknown(long relatedUnknown) {
		this.relatedUnknown = relatedUnknown;
	}

	public List<CollectedFromType> getDatasources() {
		return datasources;
	}

	public void setDatasources(List<CollectedFromType> datasources) {
		this.datasources = datasources;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
}
