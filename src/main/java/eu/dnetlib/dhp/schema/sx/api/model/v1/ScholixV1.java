package eu.dnetlib.dhp.schema.sx.api.model.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.scholix.Scholix;
import io.swagger.v3.oas.annotations.media.Schema;

public class ScholixV1 implements Serializable {

    @JsonProperty("linkProvider")
    private List<ScholixProvider> linkProvider;

    @JsonProperty("publicationDate")
    private String publicationDate;

    @NotBlank
    @JsonProperty("relationship")
    private ScholixRelationship relationship;

    @NotBlank
    @JsonProperty("source")
    private ScholixItem source;

    @NotBlank
    @JsonProperty("target")
    private ScholixItem target;



    public ScholixV1 linkProvider (final List<ScholixProvider> linkProvider ) {
        this.linkProvider = linkProvider;
        return this;
    }

    @Schema(description = "An entity responsible for making this record available online")
    public List<ScholixProvider> getLinkProvider() {
        return linkProvider;
    }

    public ScholixV1 addLinkProviderItem(ScholixProvider linkProviderItem) {
        if (this.linkProvider == null) {
            this.linkProvider = new ArrayList<>();
        }
        this.linkProvider.add(linkProviderItem);
        return this;
    }

    public void setLinkProvider(List<ScholixProvider> linkProvider) {
        this.linkProvider = linkProvider;
    }

    @Schema(description = "date of formal issuance (e.g., publication) of the resource; generally different from source object and target object publication dates")
    public String getPublicationDate() {
        return publicationDate;
    }

    public ScholixV1 publicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
        return this;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Schema(description = "Semantics of the relationship from source to target")
    public ScholixRelationship getRelationship() {
        return relationship;
    }

    public ScholixV1 relationship(ScholixRelationship relationship) {
        this.relationship = relationship;
        return this;
    }

    public void setRelationship(ScholixRelationship relationship) {
        this.relationship = relationship;
    }

    @Schema(description = "Root element relative to all properties describing the link’s source object.")
    public ScholixItem getSource() {
        return source;
    }


    public ScholixV1 source(ScholixItem source) {
        this.source = source;
        return this;
    }
    public void setSource(ScholixItem source) {
        this.source = source;
    }

    @Schema(description = "Root element relative to all properties describing the link’s target object.")
    public ScholixItem getTarget() {
        return target;
    }

    public ScholixV1 target(ScholixItem target) {
        this.target = target;
        return this;
    }

    public void setTarget(ScholixItem target) {
        this.target = target;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ScholixV1 scholixV1 = (ScholixV1) o;
        return Objects.equals(this.linkProvider, scholixV1.getLinkProvider()) &&
                Objects.equals(this.publicationDate, scholixV1.getPublicationDate()) &&
                Objects.equals(this.relationship, scholixV1.getRelationship()) &&
                Objects.equals(this.source, scholixV1.getSource()) &&
                Objects.equals(this.target, scholixV1.getTarget());
    }

    @Override
    public int hashCode() {
        return Objects.hash(linkProvider, publicationDate, relationship, source, target);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ScholixV1 {\n");

        sb.append("    linkProvider: ").append(toIndentedString(linkProvider)).append("\n");
        sb.append("    publicationDate: ").append(toIndentedString(publicationDate)).append("\n");
        sb.append("    relationship: ").append(toIndentedString(relationship)).append("\n");
        sb.append("    source: ").append(toIndentedString(source)).append("\n");
        sb.append("    target: ").append(toIndentedString(target)).append("\n");

        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }


    public static ScholixV1 fromScholix(Scholix input) {
        if (input == null)
            return null;
        final ScholixV1 result = new ScholixV1();


        if (input.getLinkprovider()!= null)
            result.setLinkProvider(input.getLinkprovider()
                    .stream()
                    .map(ScholixProvider::fromScholixEntityId)
                    .collect(Collectors.toList()));

        result.setPublicationDate(input.getPublicationDate());

        result.setRelationship(ScholixRelationship.fromScholixIndexRelationship(input.getRelationship()));

        result.setSource(ScholixItem.fromScholixResource(input.getSource()));
        result.setTarget(ScholixItem.fromScholixResource(input.getTarget()));
        return result;
    }
}
