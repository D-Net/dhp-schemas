package eu.dnetlib.dhp.schema.sx.api.model.v3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.dnetlib.dhp.schema.sx.api.model.v2.ScholixCreatorType;
import eu.dnetlib.dhp.schema.sx.api.model.v2.ScholixIdentifierType;
import eu.dnetlib.dhp.schema.sx.api.model.v2.ScholixLinkProviderType;
import eu.dnetlib.dhp.schema.sx.scholix.ScholixResource;

/**
 * The type Scholix item type.
 */
public class ScholixItemType {

    @JsonProperty("Identifier")
    private List<ScholixIdentifierType> identifier = new ArrayList();

    @JsonProperty("Title")
    private String title;

    @JsonProperty("Type")
    private String type;

    @JsonProperty("subType")
    private String subType;

    @JsonProperty("Creator")
    private List<ScholixCreatorType> creator = new ArrayList<>();

    @JsonProperty("PublicationDate")
    private String publicationDate;

    @JsonProperty("Publisher")
    private List<ScholixLinkProviderType> publisher = new ArrayList<>();


    /**
     * Gets identifier.
     *
     * @return the identifier
     */
    public List<ScholixIdentifierType> getIdentifier() {
        return identifier;
    }

    /**
     * Sets identifier.
     *
     * @param identifier the identifier
     * @return the identifier
     */
    public ScholixItemType setIdentifier(List<ScholixIdentifierType> identifier) {
        this.identifier = identifier;
        return this;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     * @return the title
     */
    public ScholixItemType setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getSubType() {
        return subType;
    }

    /**
     * Sets sub Type.
     *
     * @param subType the subType
     * @return the title
     */
    public ScholixItemType setSubType(String subType) {
        this.subType = subType;
        return this;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }


    /**
     * Sets type.
     *
     * @param type the type
     * @return the type
     */
    public ScholixItemType setType(String type) {
        this.type = type;
        return this;
    }

    /**
     * Gets creator.
     *
     * @return the creator
     */
    public List<ScholixCreatorType> getCreator() {
        return creator;
    }

    /**
     * Sets creator.
     *
     * @param creator the creator
     * @return the creator
     */
    public ScholixItemType setCreator(List<ScholixCreatorType> creator) {
        this.creator = creator;
        return this;
    }

    /**
     * Gets publication date.
     *
     * @return the publication date
     */
    public String getPublicationDate() {
        return publicationDate;
    }

    /**
     * Sets publication date.
     *
     * @param publicationDate the publication date
     * @return the publication date
     */
    public ScholixItemType setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
        return this;
    }

    /**
     * Gets publisher.
     *
     * @return the publisher
     */
    public List<ScholixLinkProviderType> getPublisher() {
        return publisher;
    }

    /**
     * Sets publisher.
     *
     * @param publisher the publisher
     * @return the publisher
     */
    public ScholixItemType setPublisher(List<ScholixLinkProviderType> publisher) {
        this.publisher = publisher;
        return this;
    }



    public static ScholixItemType fromScholixResource(final ScholixResource input) {
        if (input == null)
            return null;
        final ScholixItemType instance = new ScholixItemType();
        instance.setType("publication".equalsIgnoreCase(input.getObjectType())?"literature": "dataset");
        instance.setTitle(input.getTitle());

        if (input.getIdentifier()!= null)
            instance.setIdentifier(input.getIdentifier()
                    .stream()
                    .map(ScholixIdentifierType::fromScholixIdentifier )
                    .collect(Collectors.toList())
            );


        if (input.getPublisher()!= null) {
            instance.setPublisher(
                    input.getPublisher().stream()
                            .map(ScholixLinkProviderType::fromScholixEntityId)
                            .collect(Collectors.toList())
            );
        }

        instance.setPublicationDate(input.getPublicationDate());
        if(input.getCreator()!=null)
            instance.setCreator(input.getCreator()
                        .stream()
                        .map(ScholixCreatorType::fromScholixEntityId)
                        .collect(Collectors.toList()));

        return instance;
    }
}
