package eu.dnetlib.dhp.schema.mdstore;

public enum ValidationType {
    openaire4_0,
    openaire3_0,
    openaire2_0,
    fair_data,
    fair_literature_v4;
}
